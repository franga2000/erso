<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Računalniki", array(
 "bootstrap" => True,
 "css" => "style.css"
));

$document->add("header", array("auth" => $a));

?><h2>Računalniki</h2>

<?php ob_start(); ?>
<script>
 function refreshAjax() {
  $.ajax({
   data: {
    o: "computers",
    q: {
     searchType: $("#searchType").val(),
     searchModel: $("#searchModel").val(),
     searchNewID: $("#searchNewID").val(),
     searchLegacyID: $("#searchLegacyID").val(),
     searchGraphics: $("#searchGraphics").val(),
     searchCPU: $("#searchCPU").val(),
     searchDisk: $("#searchDisk").val(),
     searchRAM: $("#searchRAM").val(),
     <?php if ($_SESSION["location"]): ?>
     searchLocation: "<?php echo $_SESSION["location"] ?>"
     <?php endif; ?>
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").html(result.html);
   }
  }); 
 }

 $('input.search').change(function () {
  refreshAjax();
 });

 $(document).ready(function() {
  refreshAjax();
 });
 
</script>
<?php $document->addJS(ob_get_clean()); ?>

<?php if($a->verify(False, "editComputers") !== False) { ?>
 <a href="computersNew.php" class="btn btn-success" target="_blank"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Nov vnos</a>
<?php } ?>

<table class="table table-striped">
 <thead>
  <tr>
   <th>Tip</th>
   <th>Model</th>
   <th>Oznaka</th>
   <th>Stara oznaka</th>
   <th>Grafična</th>
   <th>CPU</th>
   <th>Disk</th>
   <th>RAM</th>
   <th>Stanje</th>
   <th></th>
   <th></th>
   <th></th>
  </tr>
  <tr>
   <td><input type="text" class="form-control search" id="searchType"></td>
   <td><input type="text" class="form-control search" id="searchModel"></td>
   <td><input type="text" class="form-control search" id="searchNewID"></td>
   <td><input type="text" class="form-control search" id="searchLegacyID"></td>
   <td><input type="text" class="form-control search" id="searchGraphics"></td>
   <td><input type="text" class="form-control search" id="searchCPU"></td>
   <td><input type="text" class="form-control search" id="searchDisk"></td>
   <td><input type="text" class="form-control search" id="searchRAM"></td>
   <td></td>
   <td colspan="3"><a href="javascript:refreshAjax();" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></td>
  </tr>
 </thead>
 <tbody id="rows">
 </tbody>
</table>
