<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "editComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Nova periferija", array(
 "bootstrap" => True,
 "css" => "style.css"
));

if($_SERVER['REQUEST_METHOD'] === 'POST') {

 require_once "interfaces/peripheralsInterface.php";
 $c = new peripheralsInterface();
 $data = $_POST;
 $data["enteredBy"] = $a->user["username"];
 $c->newEntry($data);
 
 ?><script>window.opener.refreshAjax(); <?php if (http_response_code() == 200) { ?>window.close();<?php } ?></script><?php

} else {

?><h1>Aplikacija RSO</h1>
<h2>Periferija</h2>

<h3>Uredi vnos</h3>
<form method="POST">
 <table class="table">
  <tbody>
   <tr>
    <th>Vrsta naprave</th>
    <td><input type="text" class="form-control" name="type" id="type"></td>
   </tr>
   <tr>
    <th>Proizvajalec</th>
    <td><input type="text" class="form-control" name="manufacturer" id="manufacturer"></td>
   </tr>
   <tr>
    <th>Model</th>
    <td><input type="text" class="form-control" name="model" id="model"></td>
   </tr>
   <tr>
    <th>Serijska</th>
    <td><input type="text" class="form-control" name="serial" id="serial"></td>
   </tr>
   <tr>
    <th>Stara oznaka</th>
    <td><input type="text" class="form-control" name="legacyID"></td>
   </tr>
   <tr>
    <th>Opombe</th>
    <td><input type="text" class="form-control" name="notes" id="notes"></td>
   </tr>
  </tbody>
  <tfoot>
   <tr>
    <td></td>
    <td>
     <button type="submit" class="btn btn-primary">
      <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Shrani
     </button>
    </td>
   </tr>
  </tfoot>
 </table>
 <input type="hidden" name="id" value="<?php echo $_GET["q"]; ?>">
</form>
<?php }
