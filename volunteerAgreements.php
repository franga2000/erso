<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewMembers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Dogovori o prostovoljstvu", array(
 "bootstrap" => True,
 "css" => "style.css"
));

if(isset($_GET["q"])) {

 require_once "interfaces/membersInterface.php";
 $m = new membersInterface();
 
 $searchName = $m->name($_GET["q"]);

 unset($m);

} else {
 $searchName = "";
}

$document->add("header", array("auth" => $a));

?><h2>Dogovori o prostovoljstvu</h2>

<?php ob_start(); ?>
<script>
 function refreshAjax() {
  $.ajax({
   data: {
    o: "volunteerAgreements",
    q: {
     searchName : $("#searchName").val(),
     searchDate : $("#searchDate").val()
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").html(result.html);
   }
  }); 
 }

 $('input.search').change(function () {
  refreshAjax();
 });
 
 function printout(data) {
  window.open("printouts.php?"+ $.param({
   o: "volunteerAgreement",
   q: data
  }), "_blank");
 }

 $(document).ready(function() {
  refreshAjax();
 });
 
</script>
<?php $document->addJS(ob_get_clean()); ?>

<table class="table table-striped">
 <thead>
  <tr>
   <th>Ime in Priimek</th>
   <th>Dogovor od</th>
   <th>Dogovor do</th>
   <th></th>
   <th></th>
  </tr>
  <tr>
   <td><input type="text" class="form-control search" id="searchName" value="<?php echo $searchName; ?>"></td>
   <td colspan="2"><input type="text" class="form-control search" id="searchDate" placeholder="<?php echo date("d. m. Y"); ?>"></td>
   <td colspan="1"><a href="javascript:refreshAjax();" class="btn btn-info"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></td>
   <td colspan="1"><a href="volunteerAgreementsNew.php?q=<?php echo $_GET["q"]; ?>" class="btn btn-success"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Ustvari</a></td>
  </tr>
 </thead>
 <tbody id="rows">
 </tbody>
</table>
