<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "editComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Urejanje računalnika", array(
 "bootstrap" => True,
 "css" => "style.css"
));

require_once "interfaces/computersInterface.php";
$c = new computersInterface();

if(!isset($_GET["q"])) {
 header("Location: index.php", true, 307); //invalid request, redirect back
} else {

if($_SERVER['REQUEST_METHOD'] === 'POST') {

 $data = $_POST;
 $data["enteredBy"] = $a->user["username"];
 $c->modifyEntry($data);
 
 ?><script>window.opener.refreshAjax(); <?php if (http_response_code() == 200) { ?>window.close();<?php } ?></script><?php

} else {

$d = $c->details($_GET["q"]);

?><h1>Aplikacija RSO</h1>
<h2>Oprema</h2>

<?php ob_start(); ?>
<script>
 function refreshAjax() {
  $.ajax({
   data: {o: "autoEntry"},
   url: "ajax.php",
   success: function(result) {
    $("#autoEntryList").html(result.html);
   }
  }); 
 }
 
 function ajaxFill(id) {
  $.ajax({
   data: {o: "autoEntryFill", q: {"id": id}},
   url: "ajax.php",
   success: function(result) {
    $.each(result.fields, function (key, value) {
     if($("#" + key).val() == "") {
      $("#" + key).val(value); 
     }
    });
   }
  }); 
 }
 
 $(document).ready(function() {
  refreshAjax();
 });
 
</script>
<?php $document->addJS(ob_get_clean()); ?>

<table class="table">
 <tbody>
  <tr> 
   <td>
    <h3>Uredi vnos</h3>
    <form method="POST">
     <table class="table">
      <tbody>
       <tr>
        <th>Tip</th>
        <td><input type="text" class="form-control" name="type" id="type" value="<?php echo $d['type']; ?>"></td>
       </tr>
       <tr>
        <th>Model</th>
        <td><input type="text" class="form-control" name="model" id="model" value="<?php echo $d['model']; ?>"></td>
       </tr>
       <tr>
        <th>Stara oznaka</th>
        <td><input type="text" class="form-control" name="legacyID" value="<?php echo $d['legacyID']; ?>"></td>
       </tr>
       <tr>
        <th>Grafična</th>
        <td><input type="text" class="form-control" name="graphics" id="graphics" value="<?php echo $d['graphics']; ?>"></td>
       </tr>
       <tr>
        <th>CPU</th>
        <td>
         <table class="table">
          <tbody>
           <tr>
            <th>Model</th>
            <td><input type="text" class="form-control" name="cpuModel" id="cpuModel" value="<?php echo $d['cpuModel']; ?>"></td>
           </tr>
           <tr>
            <th>Frekvenca</th>
            <td><input type="text" class="form-control" name="cpuSpeed" id="cpuSpeed" value="<?php echo $d['cpuSpeed']; ?>"></td>
           </tr>
           <tr>
            <th>Cache</th>
            <td><input type="text" class="form-control" name="cpuCache" id="cpuCache" value="<?php echo $d['cpuCache']; ?>"></td>
           </tr>
          </tbody>
         </table>
        </td>
       </tr>
       <tr>
        <th>Disk</th>
        <td>
         <table class="table">
          <tbody>
           <tr>
            <th>Model</th>
            <td><input type="text" class="form-control" name="diskModel" id="diskModel" value="<?php echo $d['diskModel']; ?>"></td>
           </tr>
           <tr>
            <th>Serijska</th>
            <td><input type="text" class="form-control" name="diskSerial" id="diskSerial" value="<?php echo $d['diskSerial']; ?>"></td>
           </tr>
           <tr>
            <th>Velikost (GB)</th>
            <td><input type="text" class="form-control" name="diskSize" id="diskSize" value="<?php echo $d['diskSize']; ?>"></td>
           </tr>
          </tbody>
         </table>    
        </td>
       </tr>
       <tr>
        <th>RAM</th>
        <td>
         <table class="table">
          <tbody>
           <tr>
            <th>Frekvenca</th>
            <td><input type="text" class="form-control" name="ramSpeed" id="ramSpeed" value="<?php echo $d['ramSpeed']; ?>"></td>
           </tr>
           <tr>
            <th>Velikost (MB)</th>
            <td><input type="text" class="form-control" name="ramSize" id="ramSize" value="<?php echo $d['ramSize']; ?>"></td>
           </tr>
          </tbody>
         </table>    
        </td>
       </tr> 
       <tr class="warning">
        <th>Lokacija</th>
        <td>
        <p class="text-warning">Pozor: če računalnik premaknete v drugo izpostavo, ga ne boste več videli</p>
         <?php
            require_once "inc/locationDropdown.php";
            locationDropdown("location", $d["location"])
         ?> 
        </td>
       </tr>
      </tbody>
      <tfoot>
       <tr>
        <td></td>
        <td>
         <button type="submit" class="btn btn-primary">
          <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Shrani
         </button>
        </td>
       </tr>
      </tfoot>
     </table>
     <input type="hidden" name="id" value="<?php echo $_GET["q"]; ?>">
    </form>
   </td>
   <td>
    <h3>Avtomatski vnos</h3>
    Prosim upoštevajte da avtomatski vnos ne prepisuje polj ki niso prazna.
    <table class="table">
     <thead>
      <tr>
       <th>#</th>
       <th>Model</th>
       <th>Čas odčitka</th>
       <th><a href="javascript:refreshAjax();" class="btn btn-info"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></th>
      </tr>
     </thead>
     <tbody id="autoEntryList">
     </tbody>
    </table>
   </td>
  </tr>
 </tbody>
</table>
<?php }}
