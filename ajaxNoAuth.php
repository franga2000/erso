<?php

header('Content-Type: application/json');
$out = array(); ob_start();

//import Query data
if(isset($_GET["q"])) {
 $q = $_GET["q"];
} else {
 $q = array();
}

//switch by Operation
if(isset($_GET["o"])) {
 if($_GET["o"] == "workHoursPresent") {
  
  require_once "interfaces/workHoursInterface.php";
  $w = new workHoursInterface();
  
  ob_start();
  foreach($w->allPresent() as $item) {
   ?>
    <tr>
     <td><?php echo $item["code"]; ?></td>
     <td><?php echo $item["name"]; ?></td>
    </tr>   
   <?php
  }
  $out["html"] = ob_get_clean();   
 
 } elseif($_GET["o"] == "workHoursLogon") {
  
  require_once "interfaces/workHoursInterface.php";
  $w = new workHoursInterface();
  
  if($q["code"] != "") {
   if(!$w->present($w->resolveCode($q["code"]))) {
    $out["operation"] = "logon";
    $w->logon($q["code"]);
   } else {
    $out["operation"] = "logoff";
    $w->logoff($q["code"]);   
   }
  }
  
  ob_start();
  foreach($w->allPresent() as $item) {
   ?>
    <tr>
     <td><?php echo $item["code"]; ?></td>
     <td><?php echo $item["name"]; ?></td>
    </tr>   
   <?php
  }
  $out["html"] = ob_get_clean();    
 }
}

//output valid JSON
echo json_encode(($out + array("debug" => ob_get_clean())));
