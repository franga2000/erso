<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewWorkHours") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Prisotnost (ure)", array(
 "bootstrap" => True,
 "css" => "style.css"
));


require_once "interfaces/workHoursInterface.php";
$w = new workHoursInterface();

$document->add("header", array("auth" => $a));

?><h2>Prisotnost (ure)</h2>

<a href="workHoursTerminal.php" target="_blank" class="btn btn-default">
 <span class="glyphicon glyphicon-barcode" aria-hidden="true"></span> Terminal za prijavo</a>

<?php ob_start(); ?>
<script>
 function refreshAjax() {
  $.ajax({
   data: {
    o: "workHours",
    q: {
     searchName : $("#searchName").val(),
     searchDate : $("#searchDate").val()
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").html(result.html);
   }
  }); 
 }

 $('input.search').change(function () {
  refreshAjax();
 });

 $(document).ready(function() {
  refreshAjax();
 });
 
</script>
<?php $document->addJS(ob_get_clean()); ?>

<table class="table table-striped">
 <thead>
  <tr>
   <th>Ime in Priimek</th>
   <th>Od</th>
   <th>Do</th>
   <th>Čas</th>
   <th></th>
  </tr>
  <tr>
   <td><input type="text" class="form-control search" id="searchName"></td>
   <td colspan="2"><input type="text" class="form-control search" id="searchDate" placeholder="<?php echo date("d. m. Y"); ?>"></td>
   <td></td>
   <td><a href="javascript:refreshAjax();" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></td>
  </tr>
 </thead>
 <tbody id="rows">
 </tbody>
</table>
