<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "editComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Nov dogovor o prostovoljstvu", array(
 "bootstrap" => True,
 "css" => "style.css"
));

if(!isset($_GET["q"])) {
 header("Location: volunteerAgreements.php", true, 307); //invalid request, redirect back
} else {

if($_SERVER['REQUEST_METHOD'] === 'POST') {

 require_once "interfaces/volunteerAgreementsInterface.php";
 $va = new volunteerAgreementsInterface();
 $va->newEntry($_POST);
 
 ob_start(); ?>
 <script>
  window.opener.refreshAjax(); 
  window.location.href = 'printouts.php?' + $.param({
   o: "volunteerAgreement",
   q: <?php echo json_encode($va->details(
    $_POST["username"], 
    $_POST["from"],
    $_POST["until"])); 
    ?>
  });
 </script>
 <?php $document->addJS(ob_get_clean()); 

} else {

 require_once "interfaces/membersInterface.php";
 $m = new membersInterface();
 
 $d = array(
  "username" => $_GET["q"],
  "name" => $m->name($_GET["q"])
 );

 unset($m); 

?><h1>Aplikacija RSO</h1>
<h2>Nov dogovor o prostovoljstvu</h2>

<h3>Uredi vnos</h3>
<form method="POST">
 <table class="table">
  <tbody>
   <tr>
    <th style="width: 20%;">Uporabniško ime</th>
    <td><input type="text" class="form-control" name="username" readonly value="<?php echo $d['username']; ?>"></td>
   </tr>
   <tr>
    <th>Ime<br><small>(obvezno)</small></th>
    <td><input type="text" class="form-control" readonly value="<?php echo $d['name']; ?>"></td>
   </tr>
   <tr>
    <th>Velja od</th>
    <td><input type="date" class="form-control" name="from" placeholder="<?php echo date('Y-m-d'); ?>" value="<?php echo date('Y-m-d'); ?>"></td>
   </tr>
   <tr>
    <th>Velja do</th>
    <td><input type="date" class="form-control" name="until" placeholder="<?php echo date('Y-m-d'); ?>" value="<?php echo date('Y-m-d', strtotime("6 MONTHS", time())); ?>"></td>
   </tr>
  </tbody>
  <tfoot>
   <tr>
    <td></td>
    <td>
     <button type="submit" class="btn btn-primary">
      <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Shrani
     </button>
    </td>
   </tr>
  </tfoot>
 </table>
 <input type="hidden" name="id" value="<?php echo $_GET["q"]; ?>">
</form>
<?php }}
