<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "import") === False) {
 die("Potrebno se je prijaviti");
}


require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Uvoz podatkov", array(
 "bootstrap" => True,
 "css" => "style.css"
));

require_once "interfaces/legacyInterface.php";
$l = new legacyInterface();


echo "<h3>Uvažam računalnike<h3>";

require_once "interfaces/computersInterface.php";
$c = new computersInterface();

foreach($l->computersAll() as $i) { 
 if(!$c->exists($i["stevilka"])) {
   
  echo "Uvažam: ".$i["stevilka"]."<br>";
  $c->newEntry($l->computersTransform($i));
  flush();
 
 }
}

unset($c);


echo "<h3>Uvažam ekrane</h3>";

require_once "interfaces/screensInterface.php";
$c = new screensInterface();

foreach($l->screensAll() as $i) { 
 if(!$c->exists($i["stevilka"])) {
   
  echo "Uvažam: ".$i["stevilka"]."<br>";
  $c->newEntry($l->screensTransform($i));
  flush();
 
 }
}

unset($c);


echo "<h3>Uvažam periferijo</h3>";

require_once "interfaces/peripheralsInterface.php";
$c = new peripheralsInterface();

foreach($l->peripheralsAll() as $i) { 
 if(!$c->exists($i["stevilka"])) {
   
  echo "Uvažam: ".$i["stevilka"]."<br>";
  $c->newEntry($l->peripheralsTransform($i));
  flush();
 
 }
}

unset($c);


echo "<h3>Uvažam člane</h3>";

require_once "interfaces/membersInterface.php";
$m = new membersInterface($a);

foreach($l->membersAll() as $i) {
 
 if(!$m->exists($i["id_uporabnik"])) { //only import nonexisting members
 
  echo "Uvažam: ".$i["id_uporabnik"]."<br>";
  $m->newEntry($l->membersTransform($i));
  flush();
    
 }
 
}

unset($m);


echo "<h3>Uvažam ure prisotnosti</h3>";

require_once "interfaces/workHoursInterface.php";
$w = new workHoursInterface();

foreach($l->workHoursAll() as $i) {
 
 if(!$w->present($i["id_uporabnik"], $i["velja_od"])) { //only import lines where the person isn't present
 
  echo "Uvažam: ".$i["id_uporabnik"]." ".$i["velja_od"]."<br>";
  $w->import($l->workHoursTransform($i));
  flush();
    
 }
 
}

unset($w);


echo "<h3>Uvažam stranke</h3>";

require_once "interfaces/donationsInterface.php";
$d = new donationsInterface();

foreach($l->usersAll() as $i) {
 
 if(!$d->exists($i["id_klient"])) { //only import nonexisting members
 
  echo "Uvažam: ".$i["id_klient"]."<br>";
  $d->newEntry($l->usersTransform($i), False);
  flush();
    
 }
 
}

unset($d);


echo "<h3>Uvažam oddaje opreme</h3>";

/*TODO rel_klient_oprema

 need to resolve id_oprema into gear in non-legacy database, maybe use stevilka / legacyID?

 handle returns, where velja_do is not null; id_oprema is null and id_oprema_brisana is not... 
 it seems in the legacy app returned equipment was assigned a new ID making it untraceable
 probably best to record the event as qc-dismantle

 into $this->log->setDonation(array("id" => $data["addGearId"], "type" => $data["addGearType"]), $userID);

*/


echo "<h3>Uvažam kontrolne dogodke</h3>";














