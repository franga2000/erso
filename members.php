<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewMembers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Člani", array(
 "bootstrap" => True,
 "css" => "style.css"
));

$document->add("header", array("auth" => $a));

?><h2>Člani</h2>

<?php ob_start(); ?>
<script>
 function refreshAjax() {
  $.ajax({
   data: {
    o: "members",
    q: {
     searchUsername : $("#searchUsername").val(),
     searchName : $("#searchName").val(),
     searchAddress : $("#searchAddress").val(),
     searchEMail : $("#searchEMail").val(),
     searchGSM : $("#searchGSM").val(),
     searchAge : $("#searchAge").val()
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").html(result.html);
   }
  }); 
 }

 $('input.search').change(function () {
  refreshAjax();
 });

 $(document).ready(function() {
  refreshAjax();
 });
 
</script>
<?php $document->addJS(ob_get_clean()); ?>
<?php if($a->verify(False, "editMembers") !== False) { ?>
 <a href="membersNew.php" class="btn btn-success" target="_blank"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Nov član</a>
 <br><br>
<?php } ?>
<p>Trenutno prijavljeni člani so označeni <label class="label label-success">zeleno</label>, prisotni pa <label class="label label-info">modro</label>.</p>
<table class="table table-striped">
 <thead>
  <tr>
   <th>Uporabniško ime</th>
   <th>Ime in Priimek</th>
   <th>Naslov</th>
   <th>eMail</th>
   <th>GSM</th>
   <th>Starost</th>
   <th></th>
   <th></th>
  </tr>
  <tr>
   <td><input type="text" class="form-control search" id="searchUsername"></td>
   <td><input type="text" class="form-control search" id="searchName"></td>
   <td><input type="text" class="form-control search" id="searchAddress"></td>
   <td><input type="text" class="form-control search" id="searchEMail"></td>
   <td><input type="text" class="form-control search" id="searchGSM"></td>
   <td><input type="text" class="form-control search" id="searchAge"></td>
   <td colspan=2><a href="javascript:refreshAjax();" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></td>
  </tr>
 </thead>
 <tbody id="rows">
 </tbody>
</table>
