<?php

//the terminal has no authentication, for now

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Terminal za prijavo na prisotnost", array(
 "bootstrap" => True,
 "css" => "style.css"
));

require_once "inc/auth.php";
$a = new auth();

$redirect = (isset($_GET["login"]) && $_GET["login"] > 0);
if($a->verify($redirect) !== False) {

 require_once "interfaces/workHoursInterface.php";
 $w = new workHoursInterface();
 
 $myCode = $w->lookupCode($a->user["username"]);
 
} else {

 $myCode = "";
 
}


?><h1>Aplikacija RSO</h1>
<h2>Prisotnost (ure)</h2>
<h3>Prijava</h3>

<?php ob_start(); ?>
<script>
 function refreshAjax() {
  $.ajax({
   data: {
    o: "workHoursPresent"
   },
   url: "ajaxNoAuth.php",
   success: function(result) {
    $("#rows").html(result.html);
   }
  }); 
 }
 
 function logon() {
  var code = $("#code").val();
  $("#code").val("<?php echo $myCode; ?>");
  if(code !== "") {
   $.ajax({
    data: {
     o: "workHoursLogon",
     q: {
      code: code
     }
    },
    url: "ajaxNoAuth.php",
    success: function(result) {
     $("#rows").html(result.html);
    }
   });
  }
 }

 $("#code").on('keyup', function (e) {
  if (e.keyCode == 13) { logon(); }
 });
 
 $(document).ready(function() {
  refreshAjax();
 });
 
</script>
<?php $document->addJS(ob_get_clean()); ?>

<table class="table" style="height: calc(100% - 350px);">
 <tbody style="height: 100%;">
  <tr style="height: 100%;">
   <td style="height: 100%;">
    <table class="table" style="height: 100%;">
     <tbody style="height: 100%;">
      <tr style="height: 100%;">
       <td>
        <input type="text" class="form-control form-control-lg" style="height: 100%; text-align: center; font-size: 50pt" id="code" value="<?php echo $myCode; ?>">
       </td>
      </tr>
     </tbody>
     <tfoot>
      <tr>
       <td style="text-align: center;">
        <button type="button" class="btn btn-primary btn-lg" onclick="logon();">
         <span class="glyphicon glyphicon-time" aria-hidden="true"></span> Prijavi / Odjavi
        </button>
       </td>
      </tr>
     </tfoot>
    </table>
   </td>
   <td>
    <div style="text-align: right; width: 100%;">
     <a href="javascript:refreshAjax();" class="btn btn-info"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a>
    </div>
    <table class="table table-striped">
     <thead>
      <tr>
       <th>Koda</th>
       <th>Ime in priimek</th>
      </tr>
     </thead>
     <tbody id="rows">
     </tbody>
     <tfoot>
      <tr>
       <td colspan="2">
        <img alt="qr-code" src="qr.png" style="max-width: 100%;" />
       </td>
      </tr>
     </tfoot>
    </table>
   </td>
  </tr>
 </tbody>
</table>
