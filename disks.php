<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Diski", array(
 "bootstrap" => True,
 "css" => "style.css"
));

$document->add("header", array("auth" => $a));

?><h2>Diski</h2>

<?php ob_start(); ?>
<script>
 function refreshAjax() {
  $.ajax({
   data: {
    o: "disks",
    q: {
     searchModel: $("#searchModel").val(),
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").html(result.html);
   }
  }); 
 }

 $('input.search').change(function () {
  refreshAjax();
 });

 $(document).ready(function() {
  refreshAjax();
 });
 
</script>
<?php $document->addJS(ob_get_clean()); ?>

<?php if($a->verify(False, "editComputers") !== False) { ?>
 <a href="disksEntry.php" class="btn btn-success" target="_blank"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Vnos diskov</a>
<?php } ?>

<table class="table table-striped">
 <thead>
  <tr>
   <th>Model</th>
   <th>Serijska</th>
   <th>Velikost</th>
   <th>Stara oznaka</th>
   <th>Stanje</th>
   <th></th>
  </tr>
  <tr>
   <td><input type="text" class="form-control search" id="searchModel"></td>
   <td></td>
   <td></td>
   <td></td>
   <td></td>
   <td colspan="3"><a href="javascript:refreshAjax();" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></td>
  </tr>
 </thead>
 <tbody id="rows">
 </tbody>
</table>
