<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Monitorji", array(
 "bootstrap" => True,
 "css" => "style.css"
));

$document->add("header", array("auth" => $a));

?><h2>Monitorji</h2>

<?php ob_start(); ?>
<script>
 function refreshAjax() {
  $.ajax({
   data: {
    o: "screens",
    q: {
     searchManufacturer: $("#searchManufactuter").val(),
     searchModel: $("#searchModel").val(),
     searchSerial: $("#searchSerial").val(),
     searchSize: $("#searchSize").val(),
     searchNewID: $("#searchNewID").val(),
     searchLegacyID: $("#searchLegacyID").val(),
     searchNotes: $("#searchNotes").val(),
     <?php if ($_SESSION["location"]): ?>
     searchLocation: "<?php echo $_SESSION["location"] ?>"
     <?php endif; ?>
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").html(result.html);
   }
  }); 
 }

 $('input.search').change(function () {
  refreshAjax();
 });

 $(document).ready(function() {
  refreshAjax();
 });
 
</script>
<?php $document->addJS(ob_get_clean()); ?>

<?php if($a->verify(False, "editComputers") !== False) { ?>
 <a href="screensNew.php" class="btn btn-success" target="_blank"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Nov vnos</a>
<?php } ?>

<table class="table table-striped">
 <thead>
  <tr>
   <th>Proizvajalec</th>
   <th>Model</th>
   <th>Serijska</th>
   <th>Diagonala</th>
   <th>Oznaka</th>
   <th>Stara oznaka</th>
   <th>Opombe</th>
   <th></th>
   <th></th>
  </tr>
  <tr>
   <td><input type="text" class="form-control search" id="searchManufacturer"></td>
   <td><input type="text" class="form-control search" id="searchModel"></td>
   <td><input type="text" class="form-control search" id="searchSerial"></td>
   <td><input type="text" class="form-control search" id="searchSize"></td>
   <td><input type="text" class="form-control search" id="searchNewID"></td>
   <td><input type="text" class="form-control search" id="searchLegacyID"></td>
   <td><input type="text" class="form-control search" id="searchNotes"></td>
   <td><a href="javascript:refreshAjax();" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></td>
   <td></td>
  </tr>
 </thead>
 <tbody id="rows">
 </tbody>
</table>
