<?php

require_once "inc/auth.php";
$a = new auth();

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - prijava", array(
 "bootstrap" => True,
 "css" => "style.css"
));

if(isset($_GET["q"])) {
 if($_GET["q"] == "logout") { 
  $a->logout();
  header("Location: login.php", true, 307);
  die("Odjavljam...");
 }
}

$authRequired = True; $message = "";

//if form is submitted
if(isset($_POST) && isset($_POST["username"]) && isset($_POST["password"])) {
 
 //$a->create($_POST["username"], $_POST["password"]); //This is just for admins to quickly create a login, not for production
 
 //try login
 if($a->login($_POST["username"], $_POST["password"]) !== False) {

  // Should this be moved to auth::login()?
  $_SESSION["location"] = $_POST["loginLocation"];
  
  // Save default location for next time
  setcookie("defaultLocation", $_SESSION["location"], time() + 10*365*24*3600); // 10-year cookie should be enough, right? :)

  //success!
  $authRequired = False;

 } else {

  //alas :(
  $message = "Prijava ni uspela!";

 }
}

if(isset($_REQUEST["return"])) {
 $return = $_REQUEST["return"];
} else {
 $return = "index.php";
}

if($authRequired) {
 ?><h1>Prijava</h1>
 <form method="POST">
  <table class="table">
   <tbody>
   <tr>
     <td>Lokacija:</td>
     <td><?php
        require_once "inc/locationDropdown.php";
        
        $lokacija = null;
        if(isset($_GET["l"])) {
           $lokacija = $_GET["l"];
        } elseif(isset($_COOKIE["defaultLocation"])) {
           $lokacija = $_COOKIE["defaultLocation"];
        }
        
        locationDropdown("loginLocation", $lokacija, "Vse lokacije")
    ?></td>
    </tr>
    <tr>
     <td>Uporabniško ime:</td>
     <td><input type="text" name="username" placeholder="Vnesi uporabniško ime"></td>
    </tr>
    <tr>
     <td>Geslo:</td>
     <td><input type="password" name="password" placeholder="Vnesi geslo"></td>
    </tr>
   </tbody>
   <tfoot>
    <tr>
     <td></td>
     <td>
      <button type="submit" class="btn btn-primary">
       <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Prijava
      </button>
      <span style="color: red;"><?php echo $message; ?></span>
     </td>
    </tr>
   </tfoot>
  </table>
  <input type="hidden" name="return" value="<?php echo $return; ?>">
 </form><?php
} else {
 header("Location: ".$return, true, 303); //not 307 because a login is a POST request and 307 will cause a POST to the return URL
}
