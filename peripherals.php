<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Periferija", array(
 "bootstrap" => True,
 "css" => "style.css"
));

$document->add("header", array("auth" => $a));

?><h2>Periferija</h2>

<?php ob_start(); ?>
<script>
 function refreshAjax() {
  $.ajax({
   data: {
    o: "peripherals",
    q: {
     searchType: $("#searchType").val(),
     searchModel: $("#searchModel").val(),
     searchSerial: $("#searchSerial").val(),
     searchNewID: $("#searchNewID").val(),
     searchLegacyID: $("#searchLegacyID").val(),
     searchNotes: $("#searchNotes").val(),     
     <?php if ($_SESSION["location"]): ?>
     searchLocation: "<?php echo $_SESSION["location"] ?>"
     <?php endif; ?>
    }
   },
   url: "ajax.php",
   success: function(result) {
    $("#rows").html(result.html);
   }
  }); 
 }

 $('input.search').change(function () {
  refreshAjax();
 });

 $(document).ready(function() {
  refreshAjax();
 });
 
</script>
<?php $document->addJS(ob_get_clean()); ?>

<?php if($a->verify(False, "editComputers") !== False) { ?>
 <a href="peripheralsNew.php" class="btn btn-success" target="_blank"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Nov vnos</a>
<?php } ?>

<table class="table table-striped">
 <thead>
  <tr>
   <th>Vrsta</th>
   <th>Proizvajalec in model</th>
   <th>Serijska</th>
   <th>Oznaka</th>
   <th>Stara oznaka</th>
   <th style="width: 25%;">Opombe</th>
   <th></th>
   <th></th>
  </tr>
  <tr>
   <td><input type="text" class="form-control search" id="searchType"></td>
   <td><input type="text" class="form-control search" id="searchModel"></td>
   <td><input type="text" class="form-control search" id="searchSerial"></td>
   <td><input type="text" class="form-control search" id="searchNewID"></td>
   <td><input type="text" class="form-control search" id="searchLegacyID"></td>
   <td><input type="text" class="form-control search" id="searchNotes"></td>
   <td colspan="2"><a href="javascript:refreshAjax();" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Osveži</a></td>
  </tr>
 </thead>
 <tbody id="rows">
 </tbody>
</table>
