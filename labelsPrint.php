<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify() === False) { //anyone can print documents, for now
 die("Potrebno se je prijaviti");
}

if(isset($_GET["o"])) {


 $document = array(
  "computers" => "Nalepke_PC",
  "other" => "Nalepke_ostalo"
 );

 require_once "interfaces/labelsInterface.php";
 $l = new labelsInterface($document[$_GET["o"]]);
 
 require_once "interfaces/labelQueueInterface.php";
 $lq = new labelQueueInterface();
 
 require_once "inc/idEncoding.php";
 $e = new idEncoding();

 if($_GET["o"] == "computers") {

  require_once "interfaces/computersInterface.php";
  $c = new computersInterface();
  
  foreach($_GET["q"]["items"] as $item) {
   $qitem = $lq->details($item)["data"];
   if($qitem["type"] == "computers") {
    $qid = $e->idEncode($qitem["gear"]);
    $citem = $c->details($qid);

    //140-6ATEIKD,ChaletOS,04.12.2018,core 2 Duo e8400,3000

    $l->add(array(
     "c-".$qid,
     "Ubuntu", //TODO OS is in the disk record
     date("j. n. Y", strtotime($citem["tds"])),
     $citem["cpuModel"],
     $citem["cpuSpeed"]
    ));
   }
  } 
   
 } elseif($_GET["o"] == "other") {

  require_once "interfaces/gearInterface.php";
  $c = new gearInterface();
 
  foreach($_GET["q"]["items"] as $item) {  
   $qitem = $lq->details($item)["data"];
   
   if($qitem["type"] == "screens" || $qitem["type"] == "peripherals") {
    $citem = $c->details($qitem);

    //70-O224CPD,LCD,17,1740
        
    if($qitem["type"] == "screens") {
     $type = "Ekran";
     $model = $citem["model"];
    } else {
     $type = $citem["type2"];
     $model = $citem["model"];
    }

    $l->add(array(
     substr($qitem["type"], 0, 1)."-".$citem["id"],
     $type,
     "", //this is kinda pointless
     $model
    ));


   }
  } 
 
 }

 
}
