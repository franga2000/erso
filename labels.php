<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "viewComputers") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Nalepke", array(
 "bootstrap" => True,
 "css" => "style.css"
));

$document->add("header", array("auth" => $a));

?><h2>Nalepke</h2>

<?php ob_start(); ?>
<script>

 function printout(type) {
  var checked = [];
  $('input[type=checkbox]:checked').each(function(i, obj) {
   checked.push($(this).val());
  });
  
  var url = "labelsPrint.php?" + $.param({
   o: type, q: {
    first: $("#first").val(),
    items: checked
   }
  });

  window.open(url, '_blank');
  
 }
 
</script>
<?php $document->addJS(ob_get_clean()); ?> 

<h3>Tiskanje</h3>
<a href="javascript:printout('computers');" class="btn btn-info"><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span> Nalepke računalnikov</a>
<a href="javascript:printout('other');" class="btn btn-info"><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span> Nalepke ostale opreme</a>

Začni od nalepke na poli: <input id="first" type="number" value="1" step="1" min="1">

<h3>Izbira vsebine</h3>
<table class="table table-striped">
 <thead>
  <tr>
   <th>Vrsta nalepke</th>
   <th>Oznaka in model</th>
   <th>Tiskaj</th>
   <th>dodano</th>
   <th>Zadnji tisk</th>
  </tr>
 </thead>
 <tbody id="rows">
  <?php 
   require_once "interfaces/labelQueueInterface.php";
   $lq = new labelQueueInterface();
    
   foreach($lq->search() as $item) { ?>
  <tr>
   <td><?php if($item["type"] == "computers") { ?>velika<?php } else { ?>mala<?php } ?></td>
   <td><?php echo $item["title"]; ?></td>
   <td><input type="checkbox" value="<?php echo $item["id"]; ?>" <?php if($item["tdsPrinted"] == "") { ?>checked<?php } ?>></td>
   <td><?php echo date("j. n. Y H:i:s", strtotime($item["tds"])); ?></td>
   <td><?php if($item["tdsPrinted"] != "") { echo date("j. n. Y H:i:s", strtotime($item["tdsPrinted"])); } else { echo "ni natisnjeno"; } ?></td>
  </tr>
  <?php } ?>
 </tbody>
</table>
