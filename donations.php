<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify(True, "donations") === False) {
 die("Potrebno se je prijaviti");
}

require_once "inc/html.php";
$document = new html("Aplikacija eRSO - Oddaja", array(
 "bootstrap" => True,
 "chosen" => True,
 "css" => "style.css"
));

/*

  * Name
  * Last name
  * Company name
  * Address
  * City
  * Birthdate
  * eMail address
  * Phone
  * Status (unemployed, etc)
  * Notes
  
  * Equipment (list)

*/

require_once "interfaces/donationsInterface.php";
$d = new donationsInterface($a);

if($_SERVER['REQUEST_METHOD'] === 'POST') {

 $data = $_POST;
 $data["enteredBy"] = $a->user["username"];
 
 if($data["id"] == 0) {
  $data["id"] = $d->newEntry($data);
 } else {
  $d->modifyEntry($data); 
 }
 
}

$addGear = False;
if(isset($_GET["q"])) {

 $e = $d->donated($_GET["q"]);
 if($e === False) {

  if($_GET["q"]["type"] == "computers") {

   require_once "interfaces/computersInterface.php";
   $c = new computersInterface();

  } elseif($_GET["q"]["type"] == "screens") {

   require_once "interfaces/screensInterface.php";
   $c = new screensInterface();

  } elseif($_GET["q"]["type"] == "peripherals") {

   require_once "interfaces/peripheralsInterface.php";
   $c = new peripheralsInterface();
  
  }
  $addGear = $c->details($_GET["q"]["id"]);
  $addGear["type"] = $_GET["q"]["type"];
  unset($c);

  $entered = "";
  $e = $d->emptyUser();
  $e["id"] = 0;
 
 } else {
  
  $entered = "disabled";
  
 }
 
 ?><h1>Aplikacija eRSO</h1><?php

} else {

 $entered = "";
 $e = $d->emptyUser();
 $e["id"] = 0;

 $document->add("header", array("auth" => $a));

}
 
 ?><h2>Stranke</h2>
 
 <?php ob_start(); ?>
 <script>
 
  function loadListOfOptions() {
   $.ajax({
    data: {
     o: "donationsUserList"
    },
    url: "ajax.php",
    success: function(result) {
     $("#search").html(result.html);
     $("#search").val('<?php echo $e["id"]; ?>');
     $("#search").trigger("chosen:updated");
    }
   });
   
  }
  
  function printout() {
   var checked = [];
   $('input[type=checkbox]:checked').each(function(i, obj) {
    checked.push($(this).val());
   });
   
   var url = "printouts.php?" + $.param({
    o: 'donation', q: {
     name: $("#name").val(),
     lastname: $("#lastname").val(),
     company: $("#company").val(),
     address: $("#address").val(),
     city: $("#city").val(),
     birthdate: $("#birthdate").val(),
     email: $("#email").val(),
     gsm: $("#gsm").val(),
     status: $("#status").val(),
     notes: $("#notes").val(),
     gear: checked
    }
   });
   <?php if($entered === "") {?>
    window.open(url, '_blank');
   <?php } else { ?>
    window.location.replace(url);
   <?php } ?>
  }

  function donationCertPrintout() {
   var checked = [];
   $('input[type=checkbox]:checked').each(function(i, obj) {
    checked.push($(this).val());
   });
   
   var url = "printouts.php?" + $.param({
    o: 'donationCertificate', q: {
     name: $("#name").val(),
     lastname: $("#lastname").val(),
     company: $("#company").val(),
     address: $("#address").val(),
     city: $("#city").val(),
     birthdate: $("#birthdate").val(),
     email: $("#email").val(),
     gsm: $("#gsm").val(),
     status: $("#status").val(),
     notes: $("#notes").val(),
     gear: checked
    }
   });
   <?php if($entered === "") {?>
    window.open(url, '_blank');
   <?php } else { ?>
    window.location.replace(url);
   <?php } ?>
  }
  
  function userDetails(id) {
   $.ajax({
    data: {
     o: "donationsUserSearch",
     q: {id: $('#search').val()}
    },
    url: "ajax.php",
    success: function(result) {
     $("#name").val(result.name);
     $("#lastname").val(result.lastname);
     $("#company").val(result.company);
     $("#address").val(result.address);
     $("#city").val(result.city);
     $("#birthdate").val(result.birthdate);
     $("#email").val(result.email);
     $("#gsm").val(result.gsm);
     $("#status").val(result.status);
     $("#notes").val(result.notes);
     $("#gear").html(result.html);
    }
   });  
  }

  function userGearOnly(id) {
   $.ajax({
    data: {
     o: "donationsUserSearch",
     q: {id: '<?php echo $e["id"]; ?>'}
    },
    url: "ajax.php",
    success: function(result) {
     $("#gear").html(result.html);
     $('input[type=checkbox]').each(function(i, obj) {
      $(this).prop('checked', true);
     });
    }
   });
  }  
  
  $(document).ready(function() {
   loadListOfOptions();
   <?php if($entered === "") {?>
    $("#search").chosen().change(userDetails);
   <?php } else { ?>
    userGearOnly();   
   <?php }  ?>
  });
 </script>
 <?php $document->addJS(ob_get_clean()); ?> 
 
 <h3>Oddaja</h3>
 <table class="table">
  <tbody>
   <tr>
    <td>
     <h4>Prejemnik</h4>
     <form method="POST">
      <table class="table">
       <thead>
        <tr>
         <th style="width: 20%;">Poišči<br>
          <small>
           <label class="label label-warning">Pozor!</label> Če ne izbereš "Nov vnos" popravljaš obstoječega prejemnika.
          </small>
         </th>
         <td>
          <select id="search" name="id" <?php echo $entered; ?>>
          </select>
         </td>
        </tr>      
       </thead>
       <tbody>
        <tr>
         <th>Ime</th>
         <td><input type="text" class="form-control" name="name" id="name" placeholder="Janez" <?php echo $entered; ?> value="<?php echo $e['name']; ?>"></td>
        </tr>
        <tr>
         <th>Priimek</th>
         <td><input type="text" class="form-control" name="lastname" id="lastname" placeholder="Sklopka" <?php echo $entered; ?> value="<?php echo $e['lastname']; ?>"></td>
        </tr>
        <tr>
         <th>Podjetje</th>
         <td><input type="text" class="form-control" name="company" id="company" placeholder="" <?php echo $entered; ?> value="<?php echo $e['company']; ?>"></td>
        </tr>
        <tr>
         <th>Naslov</th>
         <td><input type="text" class="form-control" name="address" id="address" placeholder="Napačna ulica 13" <?php echo $entered; ?> value="<?php echo $e['address']; ?>"></td>
        </tr>
        <tr>
         <th>Mesto</th>
         <td><input type="text" class="form-control" name="city" id="city" placeholder="1234 Neka poljana" <?php echo $entered; ?> value="<?php echo $e['city']; ?>"></td>
        </tr>
        <tr>
         <th>Rojstni datum</th>
         <td><input type="date" class="form-control" name="birthdate" id="birthdate" <?php echo $entered; ?> value="<?php $tmpBirthdate = strtotime($e['birthdate']); if($tmpBirthdate > 0) { echo date("Y-m-d", $tmpBirthdate); } ?>"></td>
        </tr>
        <tr>
         <th>eMail naslov</th>
         <td><input type="text" class="form-control" name="email" id="email" placeholder="janez.sklopka@gmail.com" <?php echo $entered; ?> value="<?php echo $e['email']; ?>"></td>
        </tr>
        <tr>
         <th>GSM</th>
         <td><input type="text" class="form-control" name="gsm" id="gsm" placeholder="040 123 456" <?php echo $entered; ?> value="<?php echo $e['gsm']; ?>"></td>
        </tr>
        <tr>
         <th>Status</th>
         <td><input type="text" class="form-control" name="status" id="status" placeholder="Brezposlen" <?php echo $entered; ?> value="<?php echo $e['status']; ?>"></td>
        </tr>
        <tr>
         <th>Opombe</th>
         <td><input type="text" class="form-control" name="notes" id="notes" placeholder="Ljubitelj korenja" <?php echo $entered; ?> value="<?php echo $e['notes']; ?>"></td>
        </tr>
       </tbody>
       <tfoot>
        <tr>
         <td></td>
         <td>
          <?php if($addGear === False && $entered === "") {?>
          <button type="submit" class="btn btn-primary">
           <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Vnesi popravek
          </button>
          <?php } elseif($addGear !== False && $entered === "") {?>
          <button type="submit" class="btn btn-primary">
           <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Vnesi prejem
          </button>
          <?php } else {?>
          <button class="btn btn-success" disabled>
           <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Vnos uspel
          </button>
          <?php } ?>
         </td>
        </tr>
       </tfoot>
      </table>
      <?php if($addGear !== False) { ?>
       <input type="hidden" name="addGearId" value="<?php echo $addGear["id"]; ?>">
       <input type="hidden" name="addGearType" value="<?php echo $addGear["type"]; ?>">
      <?php } ?>      
     </form>
    </td>
    <td>
     <h4>Oprema</h4>
     <table class="table">
      <thead>
       <tr>
        <th>Oznaka</th>
        <th>Model</th>
        <th>Tip</th>
        <th>Izpis</th>
       </tr>
      </thead>
      <tbody id="gear">
      </tbody>
      <tfoot>
       <?php if($addGear !== False) { ?>
       <tr>
        <td colspan="4">Dodal se bo vnos:</td>
       </tr>
       <tr>
        <td><?php echo $addGear["id"]; ?></td>
        <td><?php echo $addGear["model"]; ?></td>
        <td><?php echo $addGear["type"]; ?></td>
        <td><input type="checkbox" value="<?php echo substr($addGear["type"], 0, 1)."-".$addGear["id"] ?>"></td>
       </tr>
       <?php } ?>
       <tr>
        <td colspan="4" style="text-align: right;">
         <a href="javascript:printout();" class="btn btn-info">
          <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Obrazec za oddajo
         </a>
         <a href="javascript:donationCertPrintout();" class="btn btn-success">
          <span class="glyphicon glyphicon-gift" aria-hidden="true"></span> Potrdilo o donaciji
         </a>
        </td>
       </tr>
      </tfoot>
     </table>
    </td>
   </tr>
  </tbody>
 </table>
