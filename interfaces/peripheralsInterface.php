<?php

require_once "inc/dblink.php";
require_once "inc/idEncoding.php";
require_once "interfaces/auditLogInterface.php";
require_once "interfaces/labelQueueInterface.php";

class peripheralsInterface {

 private $db;
 private $e;
 private $log;
 private $columns = array(
   "type" => 45,
   "manufacturer" => 45,
   "model" => 45,
   "serial" => 45,
   "legacyID" => 15,
   "notes" => 255,
   "enteredBy" => 45,
   "location" => 15,
   "tds" => 200
  );
 
 private $peripheralsCache;
 
 /*

   CREATE TABLE `peripherals` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `type` varchar(45) DEFAULT NULL,
    `manufacturer` varchar(45) DEFAULT NULL,
    `model` varchar(45) DEFAULT NULL,
    `serial` varchar(45) DEFAULT NULL,
    `legacyID` varchar(15) DEFAULT NULL,
    `notes` varchar(255) DEFAULT NULL,
    `location` varchar(15) NOT NULL,
    `tds` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    `enteredBy` varchar(45) DEFAULT NULL,
    PRIMARY KEY (`id`)
   ) ENGINE=InnoDB AUTO_INCREMENT=355 DEFAULT CHARSET=utf8;
   
 */

 function __construct() {
  $this->db = new dblink();
  $this->e = new idEncoding();
  $this->log = new auditLogInterface();  
 }
  
 private function conversions($item) {
  //do data type conversions here
  $item["id"] = $this->e->idEncode($item["id"]); //this is alphanumeric outside and an intiger in the database
  $item["type2"] = $item["type"];

  return $item; 
 }
 
 public function details($id) {
  $out = False;
  
  //foreach does nothing if no rows returned
  foreach($this->db->q("
   SELECT * FROM `peripherals`
    WHERE `id` = '".$this->db->e($this->e->idDecode($id))."'
    LIMIT 1
  ") as $item) { $out = $this->conversions($item); }
  
  return $out;
 }
 
 public function search($query = False, $maxResults = 500) {
 
  //build search conditions
  $where = array("TRUE"); //at least one element
  if($query !== False && count($query) > 0) {
   foreach(array(
    "searchType" => "`type`", 
    "searchModel" => "CONCAT(`manufacturer`,' ',`model`)",
    "searchNewID" => "`id`", 
    "searchLegacyID" => "`legacyID`",
    "searchSerial" => "`serial`", 
    "searchNotes" => "`notes`", 
    "searchLocation" => "`location`",
   ) as $js => $sql) {
    if(isset($query[$js]) && $query[$js] != "") {
    
     //this is alphanumeric outside and an intiger in the database
     if($js == "searchNewID") {
	  $where[] = "CONV(".$sql.", 10, 32) LIKE '%".$this->db->e($query[$js])."%'";
     }else{
      $where[] = $sql." LIKE '%".$this->db->e($query[$js])."%'";
     }
     
    }
   }
  }
  
  $out = array();
  foreach($this->db->q("
   SELECT * FROM `peripherals`
    WHERE ".implode(" AND ", $where)."
    ORDER BY `tds` DESC
    LIMIT ".$this->db->e($maxResults)."
  ") as $item) { $out[] = $this->conversions($item); }
  
  return $out;
 
 }
 
 public function newEntry($data) {
  
  if(isset($this->peripheralsCache) && isset($data["legacyID"]) && $data["legacyID"] != '') {
   $this->peripheralsCache[] = $data["legacyID"];
  }  
  
  $values = array();
  foreach($this->columns as $column => $len) {
   if(isset($data[$column])) { //skip columns where data is missing -- let the database guess
    $values[$column] = $this->db->e(str_replace(array("\n", "\r"), " ", substr($data[$column], 0, $len)));
   }
  }

  if (!isset($values["location"]))
    $values["location"] = $_SESSION["location"];

  $this->db->insert("peripherals", $values);  

  if(isset($data["tds"])) { $tds = $data["tds"]; } else { $tds = False; }
  $this->log->newEntry("peripherals", $this->db->link->insert_id, $tds);

  $lq = new labelQueueInterface();
  $lq->newEntry(array("type" => "peripherals", "gear" => $this->db->link->insert_id), "other", "p-".$this->e->idEncode($this->db->link->insert_id)." - ".$data["model"]);
 
 }

 public function modifyEntry($data) {
   
  $set = array();
  foreach($this->columns as $column => $len) {
   if(isset($data[$column])) { //skip columns where data is missing -- let the database guess
    if(!in_array($column, array("id"))) {
     $set[] = "`".$column."` = '".$this->db->e(substr($data[$column], 0, $len))."'";
    }
   }
  }

  $decodedId = $this->e->idDecode($data["id"]);
 
  $this->db->q("
   UPDATE `peripherals`
      SET ".implode(", ", $set)."
    WHERE `id` = '".$this->db->e($decodedId)."'
  ");
  
  $this->log->modifyEntry("peripherals", $decodedId);
   
 }
 
 public function exists($legacyID) {
  if(!isset($this->peripheralsCache)) {
   $this->peripheralsCache = array();
   foreach($this->db->q("SELECT `legacyID` FROM `peripherals`") as $item) {
    $this->peripheralsCache[] = $item["legacyID"];
   }
  }
  return ($legacyID === '' || in_array($legacyID, $this->peripheralsCache));
 } 
 
 public function usernameChange($old, $new) {
  $this->db->q("
   UPDATE `peripherals`
      SET `enteredBy` = '".$this->db->e($new)."'
    WHERE `enteredBy` = '".$this->db->e($old)."' 
  ");
 }

}
