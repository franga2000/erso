<?php

/* 

 NOTE: This script requires:
  * glables
  
 This script is designed to work with GLabel templates.
 
 Outputs a PDF

*/

require_once "pdfconfig.php";

class labelsInterface {

 private $sourceName;
 private $glabelsFile;
 private $temporaryPrefix;
 private $data;
 
 //$l->first = 4;
 public $first = 1;

 //$l = new labelsInterface("glabels_filename_with_no_extension");
 function __construct($sourceFile, $temporaryPrefix = "/tmp/pdf-document-", $sourceLocation = "") {
 
  if($sourceLocation == "") {
   $c = new pdfconfig();
   $sourceLocation = $c->sourceLocation;
  }
  
  $this->sourceName = $sourceFile;
  $this->temporaryPrefix = $temporaryPrefix;
  $this->glabelsFile = $sourceLocation."/".$sourceFile.".glabels";
  
 }
 
 //$l->add(array("first column", "second column", "third column"));
 public function add($array) {
  $this->data .= implode(",", $array)."\n";
 }

 //fancy exec implementation that accepts fancy stdin data
 private function advancedExec($cmd, $stdin = "") {
   
  $process = proc_open($cmd, array(0 => array("pipe", "r")), $pipes);

  if(is_resource($process)) {
   fwrite($pipes[0], $stdin);
   fclose($pipes[0]);

   return proc_close($process);
  } else {
   return False;
  }
  
 }

 //outputs PDF file on destruct automatically
 function __destruct() {
  header("Content-Type: application/pdf");
  header('Content-Disposition: inline; filename="'.rawurlencode($this->sourceName).'.pdf"');
  //glabels currently outputs giberish to both stdout and stderr making it impossible to filter out the PDF file when using either, only possible to use a temporary file
  $temporaryFile = $this->temporaryPrefix.uniqid().".pdf";
  $this->advancedExec("glabels-3-batch -f ".escapeshellarg($this->first)." -i /dev/stdin -o ".escapeshellarg($temporaryFile)." ".escapeshellarg($this->glabelsFile), $this->data);
  readfile($temporaryFile);
  unlink($temporaryFile);
 }  

}
