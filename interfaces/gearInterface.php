<?php

require_once "inc/idEncoding.php";
require_once "interfaces/computersInterface.php";
require_once "interfaces/screensInterface.php";
require_once "interfaces/peripheralsInterface.php";

class gearInterface {

 private $interfaces;
 private $e;
 
 function __construct() {
  $this->e = new idEncoding();
 }
 
 private function loadInterfaces() {
  if(!isset($this->interfaces)) {
   $this->interfaces["computers"] = new computersInterface();
   $this->interfaces["screens"] = new screensInterface();
   $this->interfaces["peripherals"] = new peripheralsInterface();
  }
 }

 function details($item) {
 
  $this->loadInterfaces();

  $out = $this->interfaces[$item["type"]]->details($this->e->idEncode($item["gear"]));
  $out["type"] = $item["type"];
  
  return $out;
 
 }
 
 function decode($item) {
  $item["id"] = $this->e->idDecode($item["id"]);
  return $item;
 }

}
