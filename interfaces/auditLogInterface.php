<?php

require_once "inc/dblink.php";
require_once "inc/auth.php";
require_once "interfaces/gearInterface.php";

class auditLogInterface {

 /*

  CREATE TABLE `auditLog` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `type` varchar(45) DEFAULT NULL,
    `gear` int(11) DEFAULT NULL,
    `transaction` varchar(45) DEFAULT NULL,
    `enteredBy` varchar(45) DEFAULT NULL,
    `user` varchar(45) DEFAULT NULL,
    `tds` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 */

 private $db;
 private $username;
 private $gear;

 function __construct($a = False) {
  $this->db = new dblink();
  
  if($a === False) {
   $a = new auth();
  }
  
  $this->username = $a->user["username"];
  
  $this->gear = new gearInterface();
  
  $this->maintenence();
  
 }
  
 function newEntry($gearType, $gear, $tds = False) { //gear ID is usually decoded here
  $data = array(
   "gear" => $gear,
   "type" => $gearType,
   "enteredBy" => $this->username,
   "transaction" => "entry"
  );
  
  if($tds !== False) {
   $data += array("tds" => $tds);
  }
 
  $this->db->insert("auditLog", $data); 
 }

 function modifyEntry($gearType, $gear) { //gear ID is usually decoded here
  $this->db->insert("auditLog", array(
   "gear" => $gear,
   "type" => $gearType,
   "enteredBy" => $this->username,
   "transaction" => "edit"
  )); 
 }

 function qcEntry($gearType, $gear, $qcStatus, $by = False) { //gear ID is usually decoded here
  if($by === False) {
   $by = $this->username;
  }
 
  $this->db->insert("auditLog", array(
   "gear" => $gear,
   "type" => $gearType,
   "enteredBy" => $by,
   "transaction" => "qc-".$qcStatus
  )); 
 }
 
 //get last state
 function status($gearId, $transaction = False) {  //gear ID is usually encoded here
  $where = array();

  if($transaction !== False) {
   $where[] = "`transaction` = '".$this->db->e($transaction)."'";
  }
 
  $where[] = "`gear` = '".$this->db->e($this->gear->decode($gearId)["id"])."'";
  $where[] = "`type` = '".$this->db->e($gearId["type"])."'";
  
  $tmp = $this->db->q("
   SELECT * FROM `auditLog`
    WHERE ".implode(" AND ", $where)."
    ORDER BY `tds` DESC
    LIMIT 1
  ");
  
  if(isset($tmp[0])) {
   return $tmp[0];
  } else {
   return False;
  }  
 }

 //record a donation to a user
 function setDonation($gearId, $userId) { //gear ID is usually encoded here
  $gearId = $this->gear->decode($gearId);
 
  $this->db->insert("auditLog", array(
   "gear" => $gearId["id"],
   "type" => $gearId["type"],
   "enteredBy" => $this->username,
   "transaction" => "donation",
   "user" => $userId
  ));
 }
 
 //get gear donated to specific user
 function getDonatedGear($userId) { 
  $out = array();
  
  if($userId > 0) {
   foreach($this->db->q("
    SELECT `type`, `gear` FROM `auditLog`
     WHERE `transaction` = 'donation'
       AND `user` = '".$this->db->e($userId)."'
     ORDER BY `tds` DESC
   ") as $item) {
    
    $out[] = $this->gear->details($item);
    
   }
  }
  
  return $out; 
 
 }
 
 //get the last user (id) to which the specified gear was donated to
 function getDonatedUser($gearId) { //gear ID is usually encoded here
  $tmp = $this->status($gearId, 'donation'); //status decodes the ID itself
    
  if($tmp !== False) {
   return $tmp["user"];
  } else {
   return False;
  }

 }

 public function usernameChange($old, $new) {
  $this->db->q("
   UPDATE `auditLog`
      SET `enteredBy` = '".$this->db->e($new)."'
    WHERE `enteredBy` = '".$this->db->e($old)."' 
  ");
 }
 
 private function maintenence() {
  $this->db->q("
   DELETE FROM `auditLog`
    WHERE `tds` < DATE_SUB(NOW(), INTERVAL 5 YEAR) 
  ");    
 }

}
