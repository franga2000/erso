<?php

require_once "inc/dblink.php";
require_once "inc/auth.php";
require_once "interfaces/workHoursInterface.php";

class membersInterface {

 private $db;
 private $columns = array(
  "username" => 45, "password" => 255, 
  "name" => 45, "lastname" => 45, "address" => 45, "city" => 45, 
  "email" => 45, "gsm" => 45, 
  "birthdate" => 99, 
  "vat" => 45, "sepa" => 45, "education" => 45, "gender" => 45, "status" => 45, "mentor" => 45, "career" => 45,
  "acl" => 255, "subsidiary" => 45,
  "notes" => 45
 );
 
 private $a; //authenticator
 private $w; //work hours interface
 
 /*

  CREATE TABLE `authentication` (
    `username` varchar(45) NOT NULL,
    `password` varchar(255) DEFAULT NULL,
    `name` varchar(45) DEFAULT NULL,
    `lastname` varchar(45) DEFAULT NULL,
    `address` varchar(45) DEFAULT NULL,
    `city` varchar(45) DEFAULT NULL,
    `email` varchar(45) DEFAULT NULL,
    `gsm` varchar(45) DEFAULT NULL,
    `birthdate` date DEFAULT NULL,
    `vat` varchar(45) DEFAULT NULL,
    `sepa` varchar(45) DEFAULT NULL,
    `education` varchar(45) DEFAULT NULL,
    `gender` varchar(45) DEFAULT NULL,
    `status` varchar(45) DEFAULT NULL,
    `mentor` varchar(45) DEFAULT NULL,
    `career` varchar(45) DEFAULT NULL,
    `acl` varchar(255) DEFAULT NULL,
    `subsidiary` varchar(45) DEFAULT NULL,
    `notes` varchar(45) DEFAULT NULL,
    `tds` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`username`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 
 */
 
 function __construct($a = False, $w = False) {
  $this->db = new dblink();
  $this->a = $a;
  $this->w = $w;
 }
 
 private function initAuth() {
  if($this->a === False) {
   $this->a = new auth();
  }   
 }

 private function initWorkHours() {
  if($this->w === False) {
   $this->w = new workHoursInterface($this); //work hours interface can use a membersInterface object
  }   
 }

 //trim username
 private function tU($u) {
  if($u !== False) {
   return substr($u, 0, 45); //45 for VARCHAR(45)
  } else {
   return False;
  }
 }
  
 private function conversions($item, $display = True) {
  //do data type conversions here
  if($display) {
   $item["name"] = trim($item["name"]." ".$item["lastname"]);
   $item["address"] = trim(trim($item["address"].", ".$item["city"],","));
   $item["age"] = floor((time() - strtotime($item["birthdate"])) / 31556926);
  }

  if($item["birthdate"] == "1970-01-01") {
   $item["birthdate"] = "";
  }
    
  $item["password"] = ($item["password"] !== NULL);
    
  $item["color"] = "";
  $item["preference"] = 1;
  if($item["password"]) {
   $this->initWorkHours();
   if($this->w->present($item["username"])) {
    $item["preference"] = 2;
    $item["color"] = "info";
   }

   $this->initAuth();
   if($this->a->loggedIn($item["username"])) {
    $item["preference"] = 3;
    $item["color"] = "success";
   }
  } else {
   $item["preference"] = 0;
   $item["color"] = "inactive";
  }
    
  return $item; 
 }
 
 public function details($username = False) {
  $username = $this->tU($username);
 
  $out = False;
  
  if($username == False) {
   $this->initAuth();
   if(isset($this->a->user["username"])) {
    $username = $this->a->user["username"];
   }
  }
  
  //foreach does nothing if no rows returned
  foreach($this->db->q("
   SELECT * FROM `authentication`
    WHERE `username` = '".$this->db->e($username)."'
    LIMIT 1
  ") as $item) { $out = $this->conversions($item, False); }
  
  return $out;
 }
 
 public function exists($username = False) {
  $username = $this->tU($username);
 
  return isset($this->details($username)["username"]);
 }
 
 //as coincidence would have it success is longer than info
 private function sortOrder($a, $b) {
  if($a["preference"] > $b["preference"]) {
   return -1;
  } elseif($a["preference"] < $b["preference"]) {
   return 1;
  } else {
  
   //no difference, sort by something else
   if($a["lastname"] > $b["lastname"]) {
    return 1;
   } elseif($a["lastname"] < $b["lastname"]) {
    return -1;
   } else {
    return 0;
   }
  }
  
 }
 
 public function search($query = False, $maxResults = 500) {
 
  //build search conditions
  $where = array("TRUE"); //at least one element
  if($query !== False && count($query) > 0) {
   foreach(array(
    "searchUsername" => "`username`",
    "searchName" => "CONCAT(`name`,' ',`lastname`)",
    "searchAddress" => "CONCAT(`address`,', ',`city`)",
    "searchEMail" => "`email`",
    "searchGSM" => "`gsm`",
    "searchAge" => "TIMESTAMPDIFF(YEAR, `birthdate`, CURDATE())", //credit: https://stackoverflow.com/questions/5773405/calculate-age-in-mysql-innodb
   ) as $js => $sql) {
    if(isset($query[$js]) && $query[$js] != "") {
    
     //this is alphanumeric outside and an intiger in the database
     if($js == "searchNewID") {
      $query[$js] = $this->idDecode($query[$js]);
     }
     
     if($js == "searchAge") {
      $where[] = $sql." = '".$this->db->e($query[$js])."'"; //match age exactly
     } else {
      $where[] = $sql." LIKE '%".$this->db->e($query[$js])."%'";
     } 
     
    }
   }
  }
  
  $out = array();
  foreach($this->db->q("
   SELECT * FROM `authentication`
    WHERE ".implode(" AND ", $where)."
    LIMIT ".$this->db->e($maxResults)."
  ") as $item) { $out[] = $this->conversions($item); }
  
  usort($out, array($this, 'sortOrder'));
  
  return $out;
 
 }
 
 public function newEntry($data) {
    
  $this->initAuth();
  $data["password"] = $this->a->encryptPassword($data["password"]);
  $data["birthdate"] = date("Y-m-d", strtotime($data["birthdate"]));
   
  $values = array();
  foreach($this->columns as $column => $len) {
   $values[] = $this->db->e(substr($data[$column], 0 , $len));
  }
 
  $this->db->q("
   INSERT INTO `authentication` (`".implode("`,`", array_keys($this->columns))."`)
   VALUES ('".implode("','", $values)."')
  ");
 
 }
 
 public function modifyEntry($data) {
    
  $data["birthdate"] = date("Y-m-d", strtotime($data["birthdate"]));
  $data["agreementDate"] = date("Y-m-d", strtotime($data["agreementDate"]));
   
  $set = array();
  foreach($this->columns as $column => $len) {
   if(!in_array($column, array("username", "password"))) {
    $set[] = "`".$column."` = '".$this->db->e(substr($data[$column], 0, $len))."'";
   }
  }
 
  $this->db->q("
   UPDATE `authentication`
      SET ".implode(", ", $set)."
    WHERE `username` = '".$this->db->e($data["username"])."'
  ");
 
 }
  
 public function changeUsername($newUsername, $oldUsername = False) {
  $newUsername = $this->tU($newUsername);
  $oldUsername = $this->tU($oldUsername);
  
  if($oldUsername == False) {
   $this->initAuth();
   if(isset($this->a->user["username"])) {
    $username = $this->a->user["username"];
   }
  }
  
  if($oldUsername !== False && $this->exists($oldUsername) && !$this->exists($newUsername)) {
   $out = $this->db->q("
    UPDATE `authentication`
       SET `username` = '".$this->db->e($newUsername)."'
     WHERE `username` = '".$this->db->e($oldUsername)."'
   ");
  } else {
   $out = False;
  }
  
  if($out) {
  
   //Call computers and workHours class and change username in those tables as well
   require_once "interfaces/computersInterface.php";   
   $tmp = new computersInterface();
   $tmp->usernameChange($oldUsername, $newUsername);
   
   $this->initWorkHours();
   $this->w->usernameChange($oldUsername, $newUsername);
   
   //TODO: Add as needed
  
  }
  
  return $out;
 }
 
 public function usernamesMatchingName($name) {
  return $this->db->flatten($this->db->q("
   SELECT `username` FROM `authentication` 
    WHERE CONCAT(`name`,' ',`lastname`) LIKE '%".$this->db->e($name)."%'
  ")); 
 }
 
 public function name($username = False) {
  $username = $this->tU($username);

  if($username == False) {
   $this->initAuth();
   if(isset($this->a->user["username"])) {
    $username = $this->a->user["username"];
   }
  }

  $out = False;
  foreach($this->db->q("
   SELECT `name`, `lastname` FROM `authentication`
    WHERE `username` = '".$this->db->e($username)."'
    LIMIT 1
  ") as $tmp) {
   $out = trim($tmp["name"]." ".$tmp["lastname"]);
  }
  
  return $out;
 }
 
}
