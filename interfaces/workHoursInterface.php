<?php

require_once "inc/dblink.php";
require_once "interfaces/membersInterface.php";

class workHoursInterface {

 private $db;
 private $m;

 /*
 
   CREATE TABLE `workHours` (
     `username` varchar(45) NOT NULL,
     `from` datetime NOT NULL,
     `until` datetime DEFAULT NULL,
     PRIMARY KEY (`username`,`from`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   
 */

 function __construct($m = False) {
  $this->db = new dblink();
  
  $this->m = $m;
  if($this->m === False) {
   $this->m = new membersInterface();
  }
  
 }

 //trim username
 private function tU($u) {
  if($u !== False) {
   return substr($u, 0, 45); //45 for VARCHAR(45)
  } else {
   return False;
  }
 }
  
 private function conversions($item) {
  //do data type conversions here
  $minutes = (strtotime($item["until"]) - strtotime($item["from"])) / 60;
  $item["interval"] = floor($minutes / 60).":".str_pad($minutes % 60, 2, "0", STR_PAD_LEFT);
  $item["name"] = $this->m->name($item["username"]);

  $item["intervalMinutes"] = $minutes;

  $item["code"] = $item["username"]; //TODO

  return $item; 
 }
 
 public function details($username, $from) { //warning: $from is in unix format
  $username = $this->tU($username);
 
  $out = False;
  
  //foreach does nothing if no rows returned
  foreach($this->db->q("
   SELECT * FROM `workHours`
    WHERE `username` = '".$this->db->e($username)."' AND `from` = '".$this->db->e(date("Y-m-d H:i:s", $from))."'
    LIMIT 1
  ") as $item) { $out = $this->conversions($item); }
  
  return $out;  
 }
 
 public function search($query = False, $maxResults = 100) {
 
  //build search conditions
  $where = array("TRUE"); //at least one element
  if($query !== False && count($query) > 0) {
  
   if(isset($query["searchName"]) && $query["searchName"] != "") {  
    $where[] = "`username` IN ('".implode("','", $this->m->usernamesMatchingName($query["searchName"]))."')";  
   }
   
   if(isset($query["searchDate"]) && $query["searchDate"] != "") {
    $where[] = "DATE(`from`) = '".date("Y-m-d", strtotime(str_replace(" ", "", $query["searchDate"])))."'";
   }

   if(isset($query["username"]) && $query["username"] != "") {
    $where[] = "`username` = '".$this->db->e($query["username"])."'";
   }

   //Note: Search is exclusive
   if((isset($query["searchFrom"]) && $query["searchUntil"] != "") && (isset($query["searchUntil"]) && $query["searchUntil"] != "")) {
    $tmpFrom = date("Y-m-d", strtotime($query["searchFrom"]))." 00:00:00";
    $tmpUntil = date("Y-m-d", strtotime($query["searchUntil"]))." 00:00:00";
    $tmpUntil = date("Y-m-d H:i:s", strtotime("-1 second", strtotime($tmpUntil))); //edge case handling
    $where[] = "`from` BETWEEN '".$this->db->e($tmpFrom)."' AND '".$this->db->e($tmpUntil)."'";
   }
   
  } 

  $out = array();
  foreach($this->db->q("
   SELECT * FROM `workHours`
    WHERE ".implode(" AND ", $where)."
    ORDER BY `from` DESC
    LIMIT ".$this->db->e($maxResults)."
  ") as $item) { $out[] = $this->conversions($item); }

  return $out;
  
 }
 
 public function modifyEntry($data) { //warning: originalFrom is in unix format
  return $this->db->q("
   UPDATE `workHours`
      SET `from` = '".$this->db->e(date("Y-m-d H:i:s", strtotime($data["from"])))."', `until` = '".$this->db->e(date("Y-m-d H:i:s", strtotime($data["until"])))."'
    WHERE `username` = '".$this->db->e($data["username"])."' AND `from` = '".$this->db->e(date("Y-m-d H:i:s", $data["originalFrom"]))."'
  ");
 }
 
 public function logon($code, $time = False) {
 
  $username = $this->resolveCode($code);

  if($time === False) {
   $from = $this->db->e(date("Y-m-d H:i:s"));
   $until = $this->db->e(date("Y-m-d H:i:s", strtotime("+1 day")));
  } else {
   $from = $this->db->e(date("Y-m-d H:i:s", strtotime($time)));
   $until = $this->db->e(date("Y-m-d H:i:s", strtotime("+1 day", strtotime($time))));
  }

  $this->db->insert("workHours", array(
   "username" => $username,
   "from" => $from,
   "until" => $until
  ));
 
 }
 
 private function lastLogon($username) {
  $username = $this->tU($username);
 
  $tmp = $this->db->q("
   SELECT `from` FROM `workHours`
    WHERE `username` = '".$this->db->e($username)."'
    ORDER BY `from` DESC
    LIMIT 1
  ");
  
  if(isset($tmp[0])) {
   return strtotime($tmp[0]["from"]);
  } else {
   return False;
  }
  
 }

 public function logoff($code, $time = False) {
 
  $username = $this->resolveCode($code);
  
  if($time === False) {
   $until = $this->db->e(date("Y-m-d H:i:s"));
  } else {
   $until = $this->db->e(date("Y-m-d H:i:s", strtotime($time)));
  }

  $from = $this->lastLogon($username);
  $this->modifyEntry(array(
   "username" => $username,
   "from" => date("Y-m-d H:i:s", $from),
   "originalFrom" => $from,
   "until" => $until
  ));
 
 }
 
 public function import($data) {
  $this->db->insert("workHours", $data);
 }
 
 //TODO: code / username
 public function resolveCode($code) {
  return $code;
 }

 public function lookupCode($username) {
  $username = $this->tU($username);
 
  return $username;
 }
 
 public function present($username, $time = False) {
  $username = $this->tU($username);
 
  if($time === False) {
   $time = $this->db->e(date("Y-m-d H:i:s"));
  } else {
   $time = $this->db->e(date("Y-m-d H:i:s", strtotime($time)));
  }
  
  $tmp = $this->db->q("
   SELECT COUNT(*) AS `members` FROM `workHours`
    WHERE `username` = '".$this->db->e($username)."'
      AND `from` <= '".$time."'
      AND `until` >= '".$time."'
  ");
  
  return ($tmp[0]["members"] > 0);
 }

 public function allPresent($time = False) {
  if($time === False) {
   $time = $this->db->e(date("Y-m-d H:i:s"));
  } else {
   $time = $this->db->e(date("Y-m-d H:i:s", strtotime($time)));
  }
  
  $out = array();
  foreach($this->db->q("
   SELECT * FROM `workHours`
    WHERE `from` <= '".$time."'
      AND `until` > '".$time."'
    ORDER BY `from` ASC
  ") as $item) { $out[] = $this->conversions($item); }
  
  return $out;
 }
 
 public function usernameChange($old, $new) {
  $this->db->q("
   UPDATE `workHours`
      SET `username` = '".$this->db->e($new)."'
    WHERE `username` = '".$this->db->e($old)."' 
  ");
 }
  
}
