<?php

require_once "inc/dblink.php";
require_once "inc/idEncoding.php";
require_once "interfaces/auditLogInterface.php";
require_once "interfaces/labelQueueInterface.php";

class computersInterface {

 private $db;
 private $e;
 private $log;
 private $columns = array(
   "type" => 45, "model" => 45, "legacyID" => 15, "graphics" => 255, 
   "cpuModel" => 45, "cpuSpeed" => 45, "cpuCache" => 45, 
   "diskModel" => 45, "diskSerial" => 45, "diskManufacturer" => 45, "diskSize" => 45, 
   "ramSpeed" => 45, "ramSize" => 45,
   "json" => 64000, "tds" => 200,
   "enteredBy" => 45, "qcBy" => 45, "location" => 15
  );
  
 private $computerCache; 
 
 /*

   CREATE TABLE `computers` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `type` varchar(45) DEFAULT NULL,
    `model` varchar(45) DEFAULT NULL,
    `legacyID` varchar(15) DEFAULT NULL,
    `graphics` varchar(255) DEFAULT NULL,
    `cpuModel` varchar(45) DEFAULT NULL,
    `cpuSpeed` varchar(45) DEFAULT NULL,
    `cpuCache` varchar(45) DEFAULT NULL,
    `diskModel` varchar(45) DEFAULT NULL,
    `diskSerial` varchar(45) DEFAULT NULL,
    `diskManufacturer` varchar(45) DEFAULT NULL,
    `diskSize` varchar(45) DEFAULT NULL,
    `ramSpeed` varchar(45) DEFAULT NULL,
    `ramSize` varchar(45) DEFAULT NULL,
    `json` text,
    `status` varchar(45) DEFAULT NULL,
    `location` varchar(15) NOT NULL,
    `tds` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    `enteredBy` varchar(45) DEFAULT NULL,
    `qcBy` varchar(45) DEFAULT NULL,
    PRIMARY KEY (`id`)
   ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

 */

 function __construct() {
  $this->db = new dblink();
  $this->e = new idEncoding();
  $this->log = new auditLogInterface();
 }
 
 private function conversions($item) {
  //do data type conversions here
  $item["status"] = array(
   "entry" => '<label class="label label-warning">Nov</label>',
   "edit" => '<label class="label label-warning">Spremenjen</label>',
   "qc-working" => '<label class="label label-success">Delujoč</label>',
   "qc-service" => '<label class="label label-info">Za servis</label>',
   "qc-dismantle" => '<label class="label label-danger">Za razgradnjo</label>',
   "donation" => '<label class="label label-success">Oddano</label>',
   "" => ""
  )[$this->log->status(array("id" => $this->e->idEncode($item["id"]), "type" => "computers"))["transaction"]];
  
  
  
  $item["id"] = $this->e->idEncode($item["id"]); //this is alphanumeric outside and an intiger in the database
  
  return $item; 
 }
 
 public function details($id) {
  $out = False;
  
  //foreach does nothing if no rows returned
  foreach($this->db->q("
   SELECT * FROM `computers`
    WHERE `id` = '".$this->db->e($this->e->idDecode($id))."'
    LIMIT 1
  ") as $item) { $out = $this->conversions($item); }
  
  return $out;
 }
 
 public function search($query = False, $maxResults = 100) {
 
  //build search conditions
  $where = array("TRUE"); //at least one element
  if($query !== False && count($query) > 0) {
   foreach(array(
    "searchType" => "`type`",
    "searchModel" => "`model`",
    "searchNewID" => "`id`", 
    "searchLegacyID" => "`legacyID`",
    "searchGraphics" => "`graphics`",
    "searchCPU" => "CONCAT(`cpuModel`,' ',`cpuSpeed`,' ',`cpuCache`)",
    "searchDisk" => "CONCAT(`diskModel`,' ',`diskSerial`,' ',`diskSize`)",
    "searchRAM" => "CONCAT(`ramSpeed`,' ',`ramSize`)",
    "searchLocation" => "`location`",

   ) as $js => $sql) {
    if(isset($query[$js]) && $query[$js] != "") {
    
     //this is alphanumeric outside and an intiger in the database
     if($js == "searchNewID") {
	  $where[] = "CONV(".$sql.", 10, 32) LIKE '%".$this->db->e($query[$js])."%'";
     }else{
      $where[] = $sql." LIKE '%".$this->db->e($query[$js])."%'";
     }
     
    }
   }
  }
  
  $out = array();
  foreach($this->db->q("
   SELECT * FROM `computers`
    WHERE ".implode(" AND ", $where)."
    ORDER BY `tds` DESC
    LIMIT ".$this->db->e($maxResults)."
  ") as $item) { $out[] = $this->conversions($item); }
  
  return $out;
 
 }
 
 public function newEntry($data) {
 
  if(isset($this->computerCache) && isset($data["legacyID"]) && $data["legacyID"] != '') {
   $this->computerCache[] = $data["legacyID"];
  }
  
  $values = array();
  foreach($this->columns as $column => $len) {
   if(isset($data[$column])) { //skip columns where data is missing -- let the database guess
    $values[$column] = $this->db->e(str_replace(array("\n", "\r"), " ", substr($data[$column], 0, $len)));
   }
  }

  if (!isset($values["location"]))
    $values["location"] = $_SESSION["location"];
  
  $this->db->insert("computers", $values);
  
  if(isset($data["tds"])) { $tds = $data["tds"]; } else { $tds = False; }
  $this->log->newEntry("computers", $this->db->link->insert_id, $tds);

  $lq = new labelQueueInterface();
  $lq->newEntry(array("type" => "computers", "gear" => $this->db->link->insert_id), "computers", "c-".$this->e->idEncode($this->db->link->insert_id)." - ".$data["model"]);
 
 }

 public function modifyEntry($data) {
   
  $set = array();
  foreach($this->columns as $column => $len) {
   if(isset($data[$column])) { //skip columns where data is missing -- let the database guess
    if(!in_array($column, array("json", "diskManufacturer", "id"))) {
     $set[] = "`".$column."` = '".$this->db->e(substr($data[$column], 0, $len))."'";
    }
   }
  }
 
  $decodedId = $this->e->idDecode($data["id"]);
 
  $this->db->q("
   UPDATE `computers`
      SET ".implode(", ", $set)."
    WHERE `id` = '".$this->db->e($decodedId)."'
  ");

  $this->log->modifyEntry("computers", $decodedId);
  
 }
 
 public function updateStatus($id, $status, $by) {
  return $this->log->qcEntry("computers", $this->e->idDecode($id), $status, $by); 
 }
 
 public function exists($legacyID) {
  if(!isset($this->computerCache)) {
   $this->computerCache = array();
   foreach($this->db->q("SELECT `legacyID` FROM `computers`") as $item) {
    $this->computerCache[] = $item["legacyID"];
   }
  }
  return ($legacyID === '' || in_array($legacyID, $this->computerCache));
 }
 
 public function usernameChange($old, $new) {
  $this->db->q("
   UPDATE `computers`
      SET `enteredBy` = '".$this->db->e($new)."'
    WHERE `enteredBy` = '".$this->db->e($old)."' 
  ");

  $this->db->q("
   UPDATE `computers`
      SET `qcBy` = '".$this->db->e($new)."'
    WHERE `qcBy` = '".$this->db->e($old)."' 
  ");
 }

}
