<?php

require_once "inc/dblink.php";
require_once "interfaces/membersInterface.php";

class volunteerAgreementsInterface {

 private $db;
 private $m;
 
 private $agreementLength = "6 MONTH"; //MySQL and PHP format

 /*
 
   CREATE TABLE `volunteerAgreements` (
     `username` varchar(45) NOT NULL,
     `from` date NOT NULL,
     `until` date NULL DEFAULT NULL,
     PRIMARY KEY (`username`,`from`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   
 */

 function __construct($m = False) {
  $this->db = new dblink();
  
  $this->m = $m;
  if($this->m === False) {
   $this->m = new membersInterface();
  }
  
 }

 //trim username
 private function tU($u) {
  if($u !== False) {
   return substr($u, 0, 45); //45 for VARCHAR(45)
  } else {
   return False;
  }
 }
  
 private function conversions($item) {
  //do data type conversions here
  $item["name"] = $this->m->name($item["username"]);
  
  return $item; 
 }
 
 public function details($username, $from, $until) {
  $username = $this->tU($username);
 
  $out = False;
  
  //foreach does nothing if no rows returned
  foreach($this->db->q("
   SELECT * FROM `volunteerAgreements`
    WHERE `username` = '".$this->db->e($username)."' 
    AND `from` = '".$this->db->e($from)."'
    AND `until` = '".$this->db->e($until)."'
    LIMIT 1
  ") as $item) { $out = $this->conversions($item); }
  
  return $out;  
 }
 
 public function search($query = False, $maxResults = 500) {
 
  //build search conditions
  $where = array("TRUE"); //at least one element
  if($query !== False && count($query) > 0) {

   if(isset($query["searchUsername"]) && $query["searchUsername"] != "") {  
    $where[] = "`username` = '".$this->db->e($query["searchUsername"])."'";  
   }
  
   if(isset($query["searchName"]) && $query["searchName"] != "") {  
    $where[] = "`username` IN ('".implode("','", $this->m->usernamesMatchingName($query["searchName"]))."')";  
   }
   
   if(isset($query["searchDate"]) && $query["searchDate"] != "") {
    $time = strtotime(str_replace(" ", "", $query["searchDate"]));
    $where[] = "'".date("Y-m-d", $time)."' BETWEEN DATE(`from`) AND DATE_ADD(`from`, INTERVAL ".$this->agreementLength.")";
    // TODO
   }
   
  } 

  $out = array();
  foreach($this->db->q("
   SELECT * FROM `volunteerAgreements`
    WHERE ".implode(" AND ", $where)."
    ORDER BY `from` DESC
    LIMIT ".$this->db->e($maxResults)."
  ") as $item) { $out[] = $this->conversions($item); }
  
  return $out;
  
 }
 
 public function valid($username, $date = False) {
  if($date === False) {
   $date = date("Y-m-d");
  }
  
  return count($this->search(array(
   "searchUsername" => $username,
   "searchDate" => $date
  ))) > 0;
 }
 
 public function newEntry($data) {
  $this->db->insert("volunteerAgreements", array(
   "username" => $this->tU($data["username"]),
   "from" => $data["from"],
   "until" => $data["until"]
  ));
 }
 
 public function modifyEntry($data) {
  // TODO
  return $this->db->q("
   UPDATE `volunteerAgreements`
      SET `from` = '".$this->db->e(date("Y-m-d", strtotime($data["from"])))."',
      SET `until` = '".$this->db->e(date("Y-m-d", strtotime($data["until"])))."'
    WHERE `username` = '".$this->db->e($data["username"])."' AND `from` = '".$this->db->e(date("Y-m-d", $data["originalFrom"]))."'
  ");
 }
  
 public function usernameChange($old, $new) {
  $this->db->q("
   UPDATE `volunteerAgreements`
      SET `username` = '".$this->db->e($new)."'
    WHERE `username` = '".$this->db->e($old)."' 
  ");
 }
  
}
