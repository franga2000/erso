<?php

require_once "inc/dblink.php";

class locationsInterface {

 private $db;
 private $columns = array(
   "id" => 15,
   "name" => 45,
  );
 
 /*

   CREATE TABLE `locations` (
    `id` varchar(15) NOT NULL AUTO_INCREMENT,
    `name` varchar(45) DEFAULT NULL,
    PRIMARY KEY (`id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 */

 function __construct() {
  $this->db = new dblink();
 }
 
 public function new($data) {

  $this->db->insert("screens", $data);  

 }

 public function delete($id) {
  $this->db-q("
   DELETE FROM `screens`
    WHERE `id` = '".$this->db->e($id)."'
  ");
 }

 public function all() {
  return $this->db->q("
    SELECT * FROM `locations`
  ", "id");
 }

 public function details($id) {
  $out = False;
  
  //foreach does nothing if no rows returned
  foreach($this->db->q("
   SELECT * FROM `locations`
    WHERE `id` = '".$id."'
    LIMIT 1
  ") as $item) { $out = $item; }
  
  return $out;
 }

}
