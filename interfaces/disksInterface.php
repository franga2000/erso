<?php

require_once "inc/dblink.php";

class disksInterface {

 private $db;

 private $columns = array(
   "diskSerial" => 45,
   "os" => 45,
   "legacyID" => 15,
   "enteredBy" => 45,
   "tds" => 200, 
  );
   
 /*

  TODO: Disks are listed in the computers table, 
   as well as a special "disks" table, where they are connected to:
    * tds (of wipe)
    * operating system
    * legacyID
    * wiped by
    
    ID is diskSerial

   there are no new entries / edits, every operation is an overwrite

  CREATE TABLE `disks` (
    `diskSerial` varchar(45) NOT NULL,
    `os` varchar(45) DEFAULT NULL,
    `legacyId` varchar(15) DEFAULT NULL,
    `enteredBy` varchar(45) DEFAULT NULL,
    `tds` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`diskSerial`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 */

 function __construct() {
  $this->db = new dblink();
 }
 
 private function conversions($item) {
  //do data type conversions here
    
  return $item; 
 }
 
 public function details($id) {
  $out = False;
  
  //foreach does nothing if no rows returned
  foreach($this->db->q("
   SELECT * FROM `disks`
    WHERE `diskSerial` = '".$this->db->e($id)."'
    LIMIT 1
  ") as $item) { $out = $this->conversions($item); }
  
  return $out;
 }
 
 public function search($query = False, $maxResults = 100) {
 
  //build search conditions
  $where = array("TRUE"); //at least one element
  if($query !== False && count($query) > 0) {
   foreach(array(
    "searchModel" => "`model`"
   ) as $js => $sql) {
    if(isset($query[$js]) && $query[$js] != "") {
         
     $where[] = $sql." LIKE '%".$this->db->e($query[$js])."%'";
     
    }
   }
  }
  
  $out = array();
  foreach($this->db->q("
   SELECT * FROM `disks`
    WHERE ".implode(" AND ", $where)."
    ORDER BY `tds` DESC
    LIMIT ".$this->db->e($maxResults)."
  ") as $item) { $out[] = $this->conversions($item); }
  
  return $out;
 
 }
 
 public function newEntry($data) {
 
  if(!isset($data["tds"])) {
   $data["tds"] = date("Y-m-d H:i:s"); //explicitly provide this if it isn't, because overwrite
  }
  
  $values = array();
  foreach($this->columns as $column => $len) {
   if(isset($data[$column])) { //skip columns where data is missing -- let the database guess
    $values[$column] = $this->db->e(str_replace(array("\n", "\r"), " ", substr($data[$column], 0, $len)));
   }
  }
  
  $update = array();
  foreach($values as $prop => $val) {
   if($prop != "diskSerial") {
    $update[] = "`".$prop."` = '".$this->db->e($val)."'";
   }
  }
 
  $this->q("
   INSERT INTO `disks` (`".implode("`,`", array_keys($values))."`)
   VALUES ('".implode("','", $this->db->ae($values))."')
   ON DUPLICATE KEY UPDATE ".implode(", ", $update)."
  "); 
  
  $this->db->insert("disks", $values);
 
 }
 
 public function usernameChange($old, $new) {
  $this->db->q("
   UPDATE `disks`
      SET `enteredBy` = '".$this->db->e($new)."'
    WHERE `enteredBy` = '".$this->db->e($old)."' 
  ");
 }

}
