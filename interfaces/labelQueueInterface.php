<?php

class labelQueueInterface {

 private $db;
 
 /*
 
  CREATE TABLE `labelQueue` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `type` varchar(45) DEFAULT NULL,
    `title` varchar(255) DEFAULT NULL,
    `data` varchar(255) DEFAULT NULL,
    `tds` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    `tdsPrinted` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;

 */
 
 function __construct() {
  $this->db = new dblink();
 }
  
 private function conversions($item) {
  //do data type conversions here
  $item["data"] = json_decode($item["data"], True);
 
  return $item;
 }
 
 public function details($id) {
  $out = False;
  
  //foreach does nothing if no rows returned
  foreach($this->db->q("
   SELECT * FROM `labelQueue`
    WHERE `id` = '".$this->db->e($id)."'
    LIMIT 1
  ") as $item) { $out = $this->conversions($item); }
  
  return $out;
 }
 
 public function search($query = False, $maxResults = 100) {
 
  //build search conditions
  $where = array("TRUE"); //at least one element
  if($query !== False && count($query) > 0) {
   foreach(array(
   ) as $js => $sql) {
    if(isset($query[$js]) && $query[$js] != "") {
     $where[] = $sql." LIKE '%".$this->db->e($query[$js])."%'";
    }
   }
  }
  
  $out = array();
  foreach($this->db->q("
   SELECT * FROM `labelQueue`
    WHERE ".implode(" AND ", $where)."
    ORDER BY `tds` DESC
    LIMIT ".$this->db->e($maxResults)."
  ") as $item) { $out[] = $this->conversions($item); }
  
  return $out;  
   
 }
 
 public function newEntry($data, $type = "other", $title = False) {
 
  if($title == False) {
   $title = $data["model"];
  }
 
  $this->db->insert("labelQueue", array(
   "type" => $type,
   "title" => $title,
   "data" => json_encode($data)
  ));
 }
 
 public function setPrinted($id, $when = False) {
  if($when == False) {
   $when = time();
  } else {
   $when = strtotime($when);
  }
  $this->db->q("
   UPDATE `labelQueue`
      SET `tdsPrinted` = '".$this->db->e(date("Y-m-d H:i:s", $when))."'
    WHERE `id` = '".$this->db->e($id)."'
  ");
 }
 
 
}
