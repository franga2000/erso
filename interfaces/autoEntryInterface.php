<?php

require_once "inc/dblink.php";

class autoEntryInterface {

 private $db;
 
 /* 

  CREATE TABLE `autoEntry` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `json` text,
   `when` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 */

 function __construct() {
  $this->db = new dblink();
 }
 
 public function add($json) {
  return $this->db->q("
   INSERT INTO `autoEntry` (`json`)
    VALUES ('".$this->db->e($json)."')
  "); 
 }
 
 public function recent() {
  $out = array();
  foreach($this->db->q("
   SELECT `id`, `json`, `when` FROM `autoEntry`
    ORDER BY `id` DESC
    LIMIT 20
  ") as $item) {
  
   $data = json_decode($item["json"], True);
   
   $out[] = array(
    "id" => $item["id"],
    "when" => $item["when"],
    "model" => $this->component($data, "Motherboard")
   );
   
  }
  return $out;
 }
 
 public function details($id) {
  $tmp = $this->db->q("
   SELECT `json` FROM `autoEntry`
    WHERE `id` = '".$this->db->e($id)."'
    LIMIT 1
  ");
  
  if(isset($tmp[0])) {
   $data = json_decode($tmp[0]["json"], True);
   
   $cpuData = $this->cpuDetails($data);
   $diskData = $this->diskDetails($data);
   $ramData = $this->ramDetails($data);
    
   return array(
    "type" => $this->type($data),
    "model" => $this->component($data, "Motherboard"),
    "graphics" => $this->component($data, "VGA compatible controller"),
    "cpuModel" => $cpuData["model"],
    "cpuSpeed" => $cpuData["speed"],
    "cpuCache" => $cpuData["cache"],
    "diskModel" => $diskData["model"],
    "diskManufacturer" => $diskData["manufacturer"],
    "diskSize" => $diskData["size"],
    "diskSerial" => $diskData["serial"],
    "ramSize" => $ramData["size"],
    "ramSpeed" => $ramData["speed"],
    "json" => json_encode($data)
   );
  } else {
   return False;
  }
 }
 
 private function recursiveSearch($haystack, $needle, $outputProperties = array("product"), $matchProperty = "description") {
  $out = False;

  if(isset($haystack[$matchProperty]) && $haystack[$matchProperty] == $needle) { //this is our item!
   $out = array();
   foreach($outputProperties as $prop) {
    if(isset($haystack[$prop])) {
     $out[$prop] = $haystack[$prop]; 
    }
   }
  } elseif(isset($haystack["children"])) { //else keep searching
   foreach($haystack["children"] as $child) {
    $out = $this->recursiveSearch($child, $needle, $outputProperties, $matchProperty);
    if($out !== False) { break; } //if one of the kids matches, bail
   }
  }
  
  return $out;
 }
 
 private function cpuDetails($data) {
  $out = array();
 
  $tmp = $this->recursiveSearch($data, "CPU", array("product", "capacity", "configuration"));
  if(isset($tmp["product"])) { 
   $out["model"] = $tmp["product"]; 
  } else {
   $out["model"] = "";
  }
  if(isset($tmp["capacity"])) { 
   $out["speed"] = ((int) ($tmp["capacity"] / 1000000))." MHz";  //MHz
  } else {
   $out["speed"] = "";
  }
     
  $out["cores"] = 0;
  if(isset($cpuData["configuration"]["enabledcores"])) {
   $out["cores"] = $cpuData["configuration"]["enabledcores"];
  }
  
  if($out["cores"] == 0 && isset($cpuData["configuration"]["cores"])) {
   $cpuCores = $cpuData["configuration"]["cores"];
  }

  if($out["cores"] == 0) { $out["cores"] = 1; }
 
  $out["cache"] = 0;

  $tmp = $this->recursiveSearch($data, "L1 cache", array("capacity"));
  if(isset($tmp["capacity"])) {
   $out["cache"] += $tmp["capacity"] * $out["cores"];
  }

  $tmp = $this->recursiveSearch($data, "L2 cache", array("capacity"));
  if(isset($tmp["capacity"])) {
   $out["cache"] += $tmp["capacity"] * $out["cores"];
  }

  $tmp = $this->recursiveSearch($data, "L3 cache", array("capacity"));
  if(isset($tmp["capacity"])) {
   $out["cache"] += $tmp["capacity"];
  }
 
  $out["cache"] = $out["cache"] / 1024;
  if($out["cache"] > 1024) {
   $out["cache"] = ((int) ($out["cache"] / 1024))." MB";   
  } else {
   $out["cache"] = ((int) $out["cache"])." KB";
  }
  
  return $out;
 }
 
 private function diskDetails($data) {
  $out = array();
  
  $tmp = $this->recursiveSearch($data, "ATA Disk", array("vendor", "product", "serial", "size"));
  if(isset($tmp["vendor"]) && isset($tmp["product"])) {
   $out["model"] = trim($tmp["vendor"]." ".$tmp["product"]);
   $out["manufacturer"] = $tmp["vendor"];
  } elseif(isset($tmp["product"])) {
   $out["model"] = trim($tmp["product"]);
   $out["manufacturer"] = "";
  } else {
   $out["model"] = "";
   $out["manufacturer"] = "";
  }
  
  if(isset($tmp["serial"])) {
   $out["serial"] = $tmp["serial"];
  } else {
   $out["serial"] = "";
  }

  if(isset($tmp["size"])) {
   $out["size"] = ((int) ($tmp["size"] / (1000*1000*1000)))." GB";
  } else {
   $out["size"] = "";
  }
  
  return $out;
 }
 
 private function component($data, $item) {
  $tmp = $this->recursiveSearch($data, $item, array("vendor", "product"));
  if(isset($tmp["vendor"]) && isset($tmp["product"])) {
   return trim($tmp["vendor"]." ".$tmp["product"]); 
  } elseif(isset($tmp["product"])) {
   return trim($tmp["product"]);
  } else {
   return False;
  }
   
 }
 
 private function ramDetails($data) {
   
  //read all memory banks (up to 7)
  for($i = 0; $i <= 6; $i++) {
   $tmp = $this->recursiveSearch($data, "bank:".$i, array("vendor", "description", "size"), "id");
   if(isset($tmp["description"]) && !stripos($tmp["description"], "empty")) {
   
    if(isset($tmp["vendor"]) && isset($tmp["product"])) {
     $ram["slot".$i]["model"] = trim($tmp["vendor"]." ".$tmp["product"]);
    } elseif(isset($tmp["product"])) {
     $ram["slot".$i]["model"] = $tmp["product"];
    } else {
     $ram["slot".$i]["model"] = "";
    }
   
    if(isset($tmp["size"])) {
     $ram["slot".$i]["size"] = ($tmp["size"] / (1024*1024));
    }

    $ram["slot".$i]["speed"] = $tmp["description"];
   }
  }
  
  //we primarily need data from the first populated bank
  reset($ram);
  $first = key($ram);
  $out = $ram[$first];
  
  //check if all populated banks are the same
  $last = $ram[$first]; $matching = True;
  foreach($ram as $slot => $d) {
   if($d != $last) { $matching = False; }
   $last = $d;
  }
  $out["banksMatch"] = $matching;
  
  //RAM size is total
  $out["size"] = 0; 
  foreach($ram as $d) { $out["size"] += $d["size"]; } 
  
  if($out["size"] > 1024) {
   $out["size"] = ((int) ($out["size"] / 1024))." GB";
  } else {
   $out["size"] = ((int) $out["size"])." MB";     
  }
  
  return $out;
 }
  
 private function type($d) {
  if(isset($d["configuration"]["chassis"])) {
   return $d["configuration"]["chassis"];
  } else {
   return False;
  }
 }

}
