<?php

/* 

 NOTE: This script requires:
  * php-zip 
  * unoconv
  
 This script is designed to work with OpenOffice writer documents 
 that have "[test]" type placeholders in the text, where dynamic 
 content is to be inserted.
 
 Outputs a PDF and can be used carelessly. :)

*/

require_once "pdfconfig.php";

class pdfInterface {

 private $zip;
 private $temporaryFile;
 
 private $content;
 
 //$pdf = new pdfInterface("file.odt")
 //can accept URLs
 function __construct($sourceFile, $temporaryPrefix = "/tmp/pdf-document-", $sourceLocation = "") {
 
  if($sourceLocation == "") {
   $c = new pdfconfig();
   $sourceLocation = $c->sourceLocation;
  }
 
  $this->temporaryFile = $temporaryPrefix.uniqid().".odt";
  
  copy($sourceLocation."/".$sourceFile, $this->temporaryFile);
  
  $this->zip = new ZipArchive();
  $this->zip->open($this->temporaryFile);
  
  $this->content = $this->zip->getFromName("content.xml");
 }
 
 //$pdf->add("test", "works!");
 public function add($constant, $data) {
  $this->content = str_ireplace("[".$constant."]", $data, $this->content);
 }
 
 //$pdf->arrayAdd(array("test" => "works!", "message" => "boo"));
 public function arrayAdd($array) {
  foreach($array as $constant => $data) {
   $this->add($constant, $data);
  }
 }
 
 //outputs PDF file on destruct and cleans up temporary file
 function __destruct() {
  $this->zip->addFromString("content.xml", $this->content);
  $this->zip->close();
  header("Content-Type: application/pdf");
  passthru("unoconv --stdout ".escapeshellarg($this->temporaryFile));
  unlink($this->temporaryFile);
 }

}
