<?php

if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    header('Content-Type: text/x-shellscript');
    function getProto()  {
        if (isset($_SERVER['HTTPS']) &&  ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || 
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') 
            return 'https://';
        else 
            return 'http://';
    }
?>#!/bin/bash

command_exists () { 
    type "$1" &> /dev/null
}

die () {
    echo "$1"
    exit 1
}

run_as_root() {
    # GUI detection 
    # TODO: Wayland!
    if [[ ! -z $DISPLAY ]]; then 
        if command_exists pkexec; then
            pkexec $@
        elif command_exists gksudo; then
            gksudo "$@"
        else
            sudo $@
        fi
    else
        sudo $@
    fi
}

command_exists curl || die "Mankja curl!"
command_exists lshw || die "Mankja lshw!"

json=$(run_as_root 'lshw -json')
if [[ $? -eq 0 ]]; then
    echo "Pošiljam podatke..."
    echo "$json" | curl -F "file=@-" <?php echo getProto() . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?> 
else
    echo "Prišlo je do napake! Podatki niso bili poslani"
fi

<?php
} else {

 require_once "interfaces/autoEntryInterface.php";
 $ae = new autoEntryInterface();
 
 header('Content-Type: application/json');
 
 $data = file_get_contents($_FILES['file']['tmp_name']);
 
// Improvizirana validacija
if (json_decode($data) === null) 
    die("[SERVER] Napaka pri dekodiranju!\n");

 $data = substr($data, strpos($data, '{')); //discard leading junk
 $data = substr($data, 0, strrpos($data, '}') + 1); //discard trailing junk
 echo json_encode($ae->add($data));

}
