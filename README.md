# About RSO

RSO is a project of recycling old computers, equipping them with Linux and donating them to people who cannot afford the purchase:
<https://racunalniki.duh-casa.si>

The project is presently active in Slovenia, but this software is made available in english, should you wish to replicate the project in another country.

# About eRSO

eRSO is an open source inventory and work hour managment software suite for the RSO project:

The software is intended to help you:
* Keep track of computer equipment
* Easily enter hardware setups of Linux computers (autoEntry.sh)
* Keep track of the members helping with your project
* Keep track of work hours being done on the project
* Help ensure every computer gets a Quality Control check
* Keep track of the donation recipients
* Simplify the documentation of donating computers

## Installation

The software is designed to run out of a LAMP installation. Set up your hosting then:

    git clone https://gitlab.com/duh-casa/erso.git

To configure the database:

    cp dbconfig.default.php dbconfig.php
    
Then edit the file and enter your MySQL database details.

Open the individual *Interface.php files to get the create statements for the individual tables. This will be simplified in the future, once alpha stage is complete.

## Using the script to upload hardware specs automatically

Download the file [autoEntry.sh](autoEntry.sh) and edit it to contain the URL of your application installation. Put the file in your Linux installation image, so that it may be used to upload specs of completed computers.

The script requires curl, so be sure the program is also included in your image.

The file is designed to require minimal changes in the event that you need to alter the script code after the script has already been distributed. Because of this **using the script outside a LAN area where DNS can be spoofed could lead to a potentially serious security problem**, therefore if you use the script in this context, please be sure to use HTTPS with a curl client that verifies the server fingerprint.