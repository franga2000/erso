<?php

require_once "inc/auth.php";
$a = new auth();
if($a->verify() === False) { //anyone can print documents, for now
 die("Potrebno se je prijaviti");
}

if(isset($_GET["o"])) {

 require_once "interfaces/pdfInterface.php";

 $document = array(
  "donation" => "Obrazec za oddajo.odt",
  "donationCertificate" => "Potrdilo o donaciji.odt",
  "volunteerAgreement" => "Dogovor o prostovoljstvu.odt",
  "timeRecord" => "Evidenca ur.odt",
  "PUDAgreement" => "Potrdilo PUD.odt"
 );

 $pdf = new pdfInterface($document[$_GET["o"]]);
 
 $q = $_GET["q"];
 $pdf->arrayAdd($q);
 
 $alternate = array();
 if($_GET["o"] == "donation") {

  require_once "interfaces/membersInterface.php";
  $m = new membersInterface($a);
  
  //do not display bad birthdates as 1. 1. 1970
  $tmpBirthdate = strtotime($q["birthdate"]);
  if($tmpBirthdate == 0) {
   $tmpBirthdate = "";
  } else {
   $tmpBirthdate = date("j. n. Y", $tmpBirthdate);
  }

  $alternate = array(
   "name recipient" => $q["name"]." ".$q["lastname"],
   "birth date" => $tmpBirthdate,
   "adress" => "address",
   "telephone" => "gsm",
   "serial numbers" => implode(", ", $q["gear"]),
   "name erso" => $m->name(),
   "timestamp" => date("j. n. Y")
  );

  unset($tmpBirthdate);

 } else if($_GET["o"] == "donationCertificate") {

  require_once "interfaces/membersInterface.php";
  $m = new membersInterface($a);

  //do not display bad birthdates as 1. 1. 1970
  $tmpBirthdate = strtotime($q["birthdate"]);
  if($tmpBirthdate == 0) {
   $tmpBirthdate = "";
  } else {
   $tmpBirthdate = date("j. n. Y", $tmpBirthdate);
  }
  
  $alternate = array(
   "name recipient" => $q["name"]." ".$q["lastname"],
   "birth date" => $tmpBirthdate,
   "adress" => "address",
   "telephone" => "gsm",
   "serial numbers" => implode(", ", $q["gear"]),
   "name erso" => $m->name(),
   "timestamp" => date("j. n. Y")
  );

  unset($tmpBirthdate);

 } elseif($_GET["o"] == "volunteerAgreement") {

  require_once "interfaces/membersInterface.php";
  $m = new membersInterface($a);
  
  $member = $m->details($q["username"]);
  if($member["mentor"] !== "") {
   $mentor = $m->details($member["mentor"]);
  } else {
   $mentor = array("name" => "", "lastname" => "", "email" => "", "gsm" => "");
  }
  
  $alternate = array(
   "PARTICIPANT_NAME" => "name",
   "PARTICIPANT_BIRTH_DATE" => date("j. n. Y", strtotime($member["birthdate"])),
   "PARTICIPANT_PHONE" => $member["gsm"],
   "PARTICIPANT_EMAIL" => $member["email"],
   "PARTICIPANT_ADRESS" => $member["address"].", ".$member["city"],
   "PARTICIPANT_GENDER" => $member["gender"],
   "PARTICIPANT_EDUCATION_LEVEL" => $member["education"],
   "PARTICIPANT_BANK_ACC_NUMBER" => $member["sepa"],
   "MENTHOR_NAME" => $mentor["name"]." ".$mentor["lastname"],
   "MENTHOR_EMAIL" => $mentor["email"],
   "MENTHOR_PHONE" => $mentor["gsm"],
   "INSTITUTE_CYTY" => "Ljubljana",
   "WORK_DATE_START" => date("j. n. Y", strtotime($q["from"])),
   "WORK_DATE_END" => date("j. n. Y", strtotime($q["until"])),
   "DATE_NOW" => date("j. n. Y")
  );
  
} else if($_GET["o"] == "timeRecord") {

  require_once "interfaces/membersInterface.php";
  $m = new membersInterface($a);
  $member = $m->details($q["username"]);

  require_once "interfaces/workHoursInterface.php";
  $w = new workHoursInterface();
  
  $from_lim = $q["from"]."-01";
  $to_lim = date("Y-m-d", strtotime("+".$q["length"]." months", strtotime($from_lim)));
  $minutes = 0;

  foreach($w->search(array(
    "username" => $q["username"],
    "searchFrom" => $from_lim,
    "searchUntil" => $to_lim
  )) as $item) {

    $minutes += $item["intervalMinutes"];
    
  }

  $hours = floor($minutes / 60);

  //do not display bad birthdates as 1. 1. 1970
  $tmpBirthdate = strtotime($member["birthdate"]);
  if($tmpBirthdate == 0) {
   $tmpBirthdate = "";
  } else {
   $tmpBirthdate = date("j. n. Y", $tmpBirthdate);
  }

  $alternate = array(
   "PARTICIPANT_NAME" => $member["name"]." ".$member["lastname"],
   "BIRTH_DATE" => $tmpBirthdate,
   "ADRESS" => $member["address"],
   "CITY" => $member["city"],
   "WORK_DATE_START" => date("n. Y", strtotime($q["from"])),
   "KODA_ZA_URE" => $hours . " ur",
   "DATE_NOW" => date("j. n. Y")
  );
 
  unset($tmpBirthdate);

} else if($_GET["o"] == "PUDAgreement") {

  require_once "interfaces/membersInterface.php";
  $m = new membersInterface($a);
  $member = $m->details($q["username"]);
  if($member["mentor"] !== "") {
    $mentor = $m->details($member["mentor"]);
  } else {
    $mentor = array("name" => "", "lastname" => "", "email" => "", "gsm" => "");
  }

  $alternate = array(
    "PARTICIPANT_NAME" => $member["name"]." ".$member["lastname"],
    "WORK_DATE_START" => date("j. n. Y", strtotime($q["from"])), 
    "MENTHOR_NAME" => $mentor["name"]." ".$mentor["lastname"],
    "DATE_NOW" => date("j. n. Y")
  );
}

 foreach($alternate as $property => $value) {
  if(isset($q[$value])) {
   $pdf->add($property, $q[$value]);
  } else {
   $pdf->add($property, $value);
  }
 }
 
}
