<?php
require_once "interfaces/locationsInterface.php";

function locationDropdown($name, $default = null, $blank = false) { 
	if (!$default) {
		if (isset($_SESSION["location"]))
			$default = $_SESSION["location"];
	}
	?>
	<select name="<?php echo $name ?>"><?php 
	if ($blank){ ?>
		<option value=""><?php echo $blank ?></option><?php
	}

	$locI = new locationsInterface();
	foreach ($locI->all() as $id => $details) {
		?>
		<option value="<?php echo $id ?>"<?php if ($id == $default) echo " selected"?>><?php echo $details["name"] . " [" . $id . "]" ?></option>
		<?php
	}
	?>
	</select>
	<?php
}

?>
