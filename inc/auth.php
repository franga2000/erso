<?php

require_once "inc/dblink.php";
/*

 CREATE TABLE `login_sessions` (
   `userID` varchar(255) NOT NULL,
   `username` varchar(45) DEFAULT NULL,
   `acl` varchar(255) DEFAULT NULL,
   `when` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`userID`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

*/

class auth {

 private $db;
 private $user;

 function __construct() {
  if(session_status() == PHP_SESSION_NONE) { session_start(); } //credit: https://stackoverflow.com/questions/6249707/check-if-php-session-has-already-started
  $this->maintenence(); 
 }
 
 function __get($var) {
  if($var == "user") {
   if(isset($this->user)) {
    return $this->user;
   } else {
    $this->verify(False, False, False);
    return $this->user;    
   }
  }
 }
 
 private function initdb() {
  if(!isset($this->db)) {
   $this->db = new dblink(); 
  }
 }
 
 //trim username
 private function tU($u) {
  if($u !== False) {
   return substr($u, 0, 45); //45 for VARCHAR(45)
  } else {
   return False;
  }
 }
 
 //create user, returns output of database interaction (success)
 public function create($username, $password) {
  $username = $this->tU($username);
  
  $this->initdb();
  $hash = $this->encryptPassword($password);
  return $this->db->q("
   INSERT INTO `authentication` (`username`, `password`)
   VALUES ('".$this->db->e($username)."', '".$this->db->e($hash)."')
   ON DUPLICATE KEY UPDATE `password` = '".$this->db->e($hash)."'
  ");
 }

 public function changePassword($password, $username = False) {
  $username = $this->tU($username);
   
  if($username == False && isset($this->user["username"])) {
   $username = $this->user["username"];
  }
  
  $this->initdb();
  if($password === False) {
   $password = "NULL";
  } else {
   $password = "'".$this->db->e($this->encryptPassword($password))."'";
  }
 
  if($username != False) {
   if($username == $this->user["username"] || $this->verify(False, "editPasswords", False)) { //additional verification that the user is authorized to change the password
    return $this->db->q("
     UPDATE `authentication` 
        SET `password` = ".$password."
      WHERE `username` = '".$this->db->e($username)."'
    ");
   } else {
    return False; //unauthorized
   }
  } else {
   return False; //no username provided and nobody is logged in
  }
 }
 
 public function encryptPassword($password) {
  return password_hash($password, PASSWORD_DEFAULT); 
 }
 
 //log user in, returns user details or False if login fails
 public function login($username, $password) {
  $username = $this->tU($username);
  
  if($this->checkPassword($password, $username) !== False) {
   $tmp[0]["session"] = $this->createSession($username);
   unset($tmp[0]["password"]);
   return $tmp[0];   
  } else {
   return False;
  }  
 }
 
 public function checkPassword($password, $username = False) {
  $username = $this->tU($username);
  
  if($username == False && isset($this->user["username"])) {
   $username = $this->user["username"];
  }
  
  if($username != False) {
   $this->initdb();
   $tmp = $this->db->q("
    SELECT * FROM `authentication`
     WHERE `username` = '".$this->db->e($username)."'
       AND `password` IS NOT NULL
     LIMIT 1
   ");
   if(isset($tmp[0])) {
    if(password_verify($password, $tmp[0]["password"])) {
     return $tmp[0];
    } else {
     return False; //password didn't match
    }
   } else {
    return False; //no such user
   }
  
  } else {
   return False; //no username given and nobody logged in
  }
 }
 
 public function enabled($username = False) {
  $username = $this->tU($username);
  
  if($username == False && isset($this->user["username"])) {
   $username = $this->user["username"];
  }

  $this->initdb();
  $tmp = $this->db->q("
   SELECT COUNT(*) AS `members` FROM `authentication`
    WHERE `username` = '".$this->db->e($username)."'
      AND `password` IS NOT NULL
  ");
  
  return ($tmp[0]["members"] > 0);
 }

 //create user session upon succesfull login, returns output of database interaction (success)
 private function createSession($username) {
  $username = $this->tU($username);
   
  $this->initdb();
  $userdata = $this->db->q("
   SELECT * FROM `authentication`
    WHERE `username` = '".$this->db->e($username)."'
      AND `password` IS NOT NULL
    LIMIT 1
  ")[0];
  
  $_SESSION['userID'] = password_hash($username.random_bytes(15), PASSWORD_DEFAULT);
  return $this->db->insert("login_sessions", array(
   "userID" => $_SESSION['userID'],
   "username" => $username,
   "acl" => $userdata["acl"]
  ));
  
 }
 
 //verify that user is logged in, returns session data or False and redirects user to login page
 public function verify($redirect = True, $acl = False, $end = True) {
  
  //is there a session key?
  if(isset($_SESSION['userID'])) {
  
   $this->initdb();
   $tmp = $this->db->q("
    SELECT * FROM `login_sessions` 
     WHERE `userID` = '".$this->db->e($_SESSION['userID'])."'
     LIMIT 1 
   ");
   
   //is session key consistent with our records?
   if(isset($tmp[0]["userID"])) {

    unset($tmp[0]["userID"]); //probably better without this?
    $this->user = $tmp[0]; //save data in variable for convenience
   
    if($acl === False || $this->checkACL($acl)) { //check if user has access to this particular page
     if($end) { unset($this->db); } //most likely won't need database after this
     return $tmp[0];
     
    } else {
    
     //no access to this page but login is valid, do nothing special
     return False;
     
    }
    
   } else {
   
    session_destroy(); //session data is invalid, remove it
    if($redirect) { header("Location: login.php?return=".rawurlencode($_SERVER['REQUEST_URI']), true, 307); }
    return False;
   }
   
  } else {
   if($redirect) { header("Location: login.php?return=".rawurlencode($_SERVER['REQUEST_URI']), true, 307); }
   return False;
  }
 }
 
 //check if user has access level
 function checkACL($acl) {
  return (strpos($this->user["acl"], "admin") !== False) || (strpos($this->user["acl"], $acl) !== False);
 }
 
 //log the user out
 public function logout($username = False) {
  $username = $this->tU($username);

  if($username == False || $username == $this->user["username"]) { //log out current user

   if(isset($_SESSION['userID'])) {
   
    $this->initdb();
    $this->db->q("
     DELETE FROM `login_sessions`
      WHERE `userID` = '".$this->db->e($_SESSION['userID'])."' 
    ");
    
   }
   
   session_destroy();
   
  } else { //log out a different user -- session will be cleaned up by login verification on next client access
  
   $this->initdb();
   $this->db->q("
    DELETE FROM `login_sessions`
     WHERE `username` = '".$this->db->e($username)."' 
   ");
  
  }
 }
 
 public function loggedIn($username) {
  $username = $this->tU($username);
  
  $this->initdb();
  $tmp = $this->db->q("
   SELECT COUNT(*) AS `sessions` FROM `login_sessions` 
    WHERE `username` = '".$this->db->e($username)."'
  ");
  return ($tmp[0]["sessions"] > 0);
 }
 
 //do maintenence on session database -- remove forgotten entries
 private function maintenence() {
  $this->initdb();
  $this->db->q("
   DELETE FROM `login_sessions`
    WHERE `when` < DATE_SUB(NOW(), INTERVAL 1 DAY) 
  ");    
 }

}
