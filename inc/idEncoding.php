<?php

class idEncoding {

 public function idDecode($id) {
  return base_convert($id, 32, 10); 
 }

 public function idEncode($id) {
  return base_convert($id, 10, 32);
 }

}
